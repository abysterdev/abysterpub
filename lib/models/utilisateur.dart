import 'dart:convert';

import 'package:abysterpub/states/authProvider.dart';
import 'package:flutter/cupertino.dart';

import '../restClient.dart';

class Utilisateur {
  int id_utilisateur;
  String email;
  String password;
  String role;
  String create_date;
  String update_date;
  String otp;
  Utilisateur();
  Utilisateur.full(this.id_utilisateur, this.email, this.password, this.role,
      this.create_date, this.update_date);
  factory Utilisateur.fromJson(Map<String, dynamic> json) {
    Utilisateur user = Utilisateur.full(
        json['user']['id_utilisateur'],
        json['user']['email'],
        json['user']['password'],
        json['user']['role'],
        json['user']['create_date'],
        json['user']['update_date']);
    return user;
  }
  AuthProvider _auth = AuthProvider();

  // Future<dynamic> authenticate() async {
  //   return RestClient.getAuthToken().then((String token) {
  //     return RestClient.procesPostRequest("user/login", this.toJson(), token)
  //         .then((Map<String, dynamic> data) {
  //       if (data.containsKey('error')) {
  //         print('Error Data : $data');
  //         return Future.value(WSErrorHandler.fromJson(data).getMessage());
  //       }

  //     });
  //   }).catchError((onError) {
  //     print('$onError');
  //     return Future.value('error');
  //   });
  // }
  static Future<Utilisateur> loadUserDataFromStorage() async {
    String userData = await RestClient.getStringFromStorage('register');
    if (userData == '') {
      return Utilisateur();
    } else {
      return Utilisateur.fromJson(json.decode(userData));
    }
  }

  Future<dynamic> getUserById() async {
    String token = await RestClient.getAuthToken();
    var data = await RestClient.procesGetRequest(
        "user/findUserById/${this.id_utilisateur}", token);
    return Future.value(data);
  }

  Future<dynamic> createUser() async {
    await RestClient.getAuthToken().then((String token) {
      Map<String, dynamic> postData = {
        "email": this.email,
        "password": this.password,
        "role": this.role
      };

      RestClient.procesPostRequest2("user/add", postData, token)
          .then((response) {
        if (response.containsKey('error')) {
          _auth.isRegistered = false;
          return Future.value('error');
        } else {
          _auth.isRegistered = true;
          RestClient.persistMapDataInStorage('register', response);
          return Future.value('success');
        }
      });
    }).catchError((onError) {
      print('$onError');
      return Future.value('error');
    });
  }

  Future<dynamic> login() {
    RestClient.getAuthToken().then((String token) {
      Map<String, dynamic> postData = {
        "email": this.email,
        "password": this.password
      };
      RestClient.procesPostRequest("user/login", postData, token)
          .then((response) {
        print(response);

        if (response != 200) {
          _auth.isLoggedIn = false;
          return Future.value('error');
        } else {
          _auth.isLoggedIn = true;
          //RestClient.persistMapDataInStorage('login', postData);
          return Future.value('success');
        }
      });
    }).catchError((onError) {
      print('$onError');
      return Future.value('error');
    });
  }

  // ignore: missing_return
  Future<dynamic> requestOtp() {
    RestClient.getAuthToken().then((String token) {
      var queryParameters = {
        'email': this.email,
      };
      var uri =
          Uri.https('$WS_URL', '/user/otp/request/$email', queryParameters);
      RestClient.procesGetRequest2(uri, token).then((response) {
        print(response);

        if (response == 200) {
          _auth.isOTPsent = true;
          return Future.value('success');
        } else {
          _auth.isOTPsent = false;
          return Future.value('error');
        }
      });
      return null;
    }).catchError((onError) {
      print('$onError');
      return Future.value('error');
    });
  }

  Future<dynamic> changePassword() {
    RestClient.getAuthToken().then((String token) {
      Map<String, dynamic> postData = {
        "email": this.email,
        "password": this.password,
        "otp": this.otp
      };
      RestClient.procesPostRequest("/user/changepassword", postData, token)
          .then((response) {
        print(response);

        if (response != 200) {
          _auth.isPasswordChanged = false;
          return Future.value('error');
        } else {
          _auth.isPasswordChanged = true;
          return Future.value('success');
        }
      });
    }).catchError((onError) {
      print('$onError');
      return Future.value('error');
    });
  }

  void updateValues(Map<String, dynamic> data) {
    this.id_utilisateur = data['user']['id_utilisateur'];
    this.email = data['user']['email'];
    this.password = data['user']['password'];
    this.role = data['user']['role'];
    this.create_date = data['user']['create_date'];
    this.update_date = data['user']['update_date'];
  }

  // Map<String, dynamic> toJson() {
  //   return <String, dynamic>{
  //     'id_utilisateur': this.id_utilisateur,
  //     'email': this.email,
  //     'password': this.password,
  //     'role': this.role,
  //     'password': this.create_date,
  //     'raisonSocial': this.update_date,
  //   };
  // }
}
