import 'dart:convert';
import 'dart:io';
import 'package:abysterpub/restClient.dart';
import 'package:abysterpub/states/facebookStates.dart';
import 'package:dio/dio.dart' as dio;
import 'campagne.dart';

const PAGE_ID = 106731911034369;

class Ads {
  String name;
  String titre;
  String cto;
  String message;
  String description;
  String status;
  Ads(
      {this.name,
      this.description,
      this.titre,
      this.cto,
      this.message,
      this.status});

  factory Ads.fromJson(Map<String, dynamic> json) {
    return Ads(
        name: json['name'],
        description: json['description'],
        titre: json['titre'],
        cto: json['cto'],
        status: json['statut'],
        message: json['message']);
  }
  FacebookStates _facebook;
  Future<Ads> createAdCreative(String path) async {
    try {
      final formData = dio.FormData.fromMap(
          {'filename': '@$path', 'access_token': ACCESS_TOKEN});
      print('@$path');
      await dio.Dio()
          .post('https://graph.facebook.com/v8.0/act_$AD_ACCOUNT_ID/adimages',
              data: formData)
          .then((response) {
        print(response.statusCode);
        print(response.data);
      });
    } on dio.DioError catch (e) {
      print(e.response.data);
    }
  }

  Future<dynamic> createAds(String name, String cto, String message) async {
    try {
      final formData = dio.FormData.fromMap({
        'name': name,
        'object_story_spec': {
          "page_id": PAGE_ID,
          "link_data": {
            "image_hash": 'fa119924ca5a2a83e35795cfc1a7ea7b',
            "link": "https://facebook.com/$PAGE_ID",
            "message": message,
            "call_to_action": {'type': cto}
          }
        },
        'access_token': ACCESS_TOKEN
      });

      final response = await dio.Dio().post(
        'https://graph.facebook.com/v8.0/act_$AD_ACCOUNT_ID/adcreatives',
        data: formData,
      );

      if (response.statusCode == HttpStatus.ok) {
        // print(response.statusCode);
        // print(response.data);
        return response.data['id'];
      } else {
        Future.value('error');
      }
    } on dio.DioError catch (e) {
      return e.response.data;
    }
  }

  Future<dynamic> createAds2(dynamic adset_id, dynamic creative_id) async {
    try {
      final formData = dio.FormData.fromMap({
        'name': 'first ads',
        'adset_id': adset_id,
        'creative': {"creative_id": creative_id},
        'status': 'PAUSED',
        'access_token': ACCESS_TOKEN
      });

      final response = await dio.Dio().post(
        'https://graph.facebook.com/v8.0/act_$AD_ACCOUNT_ID/ads',
        data: formData,
      );

      if (response.statusCode == HttpStatus.ok) {
        return response.data;
      } else {
        return Future.value('error');
      }
    } on dio.DioError catch (e) {
      print(e.response.data);
    }
  }

  Future<dynamic> saveAds() async {
    return RestClient.getAuthToken().then((String token) {
      return RestClient.procesPostRequest(
              "/publicite/add", this.toJson(), token)
          .then((value) {
        if (value != 200) {
          return Future.value("error");
        } else {
          return Future.value("success");
        }
      });
    });
  }

  Future<List<Ads>> getAllUserAds(int id) async {
    String token = await RestClient.getAuthToken();
    var data =
        await RestClient.procesGetRequest("user/findByUserById/$id", token);
    List<Ads> pubs = [];

    if (data is Map && data.containsKey("pub")) {
      for (var item in data['pub']) {
        Ads pub = Ads.fromJson(item);
        pubs.add(pub);
      }
    }
    _facebook.adsLength = pubs;
    return pubs;
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'nom': this.name,
      'message': this.message,
      'titre': this.titre,
      'cto': this.cto,
      'statut': 'PAUSED',
      'description': this.description
    };
  }
}
