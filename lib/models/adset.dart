import 'dart:convert';
import 'dart:io';
import 'package:abysterpub/restClient.dart';
import 'package:abysterpub/states/facebookStates.dart';
import 'package:dio/dio.dart' as dio;

import 'campagne.dart';

class AdSet {
  int id;
  String name;
  String optimization_goal;
  String billing_event;
  String bid_amount;
  String budget;
  String type_budget;
  String campaign_id;
  Map<String, dynamic> targeting;
  DateTime start_time;
  DateTime end_time;
  String status;

  AdSet({
    this.id,
    this.name,
    this.optimization_goal,
    this.billing_event,
    this.bid_amount,
    this.budget,
    this.type_budget,
    this.campaign_id,
    this.targeting,
    this.start_time,
    this.end_time,
    this.status,
  });

  factory AdSet.fromJson(Map<String, dynamic> json) {
    return AdSet(
      name: json['name'],
      optimization_goal: json['optimization_goal'],
      billing_event: json['billing_event'],
      bid_amount: json['bid_amount'],
      budget: json['budget'],
      type_budget: json['type_budget'],
      campaign_id: json['campaign_id'],
      targeting: json['targeting'],
      start_time: json['start_time'],
      end_time: json['end_time'],
      status: json['status'],
      id: json['adset_id'],
    );
  }
  Future<dynamic> createAdSet(
      String name,
      String optimization_goal,
      String billing_event,
      String bid_amount,
      String type_budget,
      String budget,
      String countryCode,
      String centreInteret,
      start_time,
      end_time,
      String status,
      dynamic campaign_id) async {
    try {
      int id = 6003139266461;
      final formData = dio.FormData.fromMap({
        'name': name,
        'optimization_goal': optimization_goal,
        'status': status,
        'billing_event': billing_event,
        'campaign_id': campaign_id,
        'targeting': {
          "geo_locations": {
            "countries": [countryCode]
          },
          "interests": [
            {"id": id, 'name': centreInteret}
          ]
        },
        'bid_amount': bid_amount,
        type_budget: budget,
        'start_time': start_time,
        'end_time': end_time,
        'access_token': ACCESS_TOKEN
      });

      final response = await dio.Dio().post(
        'https://graph.facebook.com/v8.0/act_$AD_ACCOUNT_ID/adsets',
        data: formData,
      );

      if (response.statusCode == HttpStatus.ok) {
        return response.data['id'];
      } else {
        return Future.value('error');
      }
    } on dio.DioError catch (e) {
      print(e.response.data);
    }
  }

  FacebookStates _facebook;

  Future<dynamic> saveAdset() async {
    return RestClient.getAuthToken().then((String token) {
      return RestClient.procesPostRequest("/adset/add", this.toJson(), token)
          .then((value) {
        if (value != 200) {
          return Future.value("error");
        } else {
          return Future.value("success");
        }
      });
    });
  }

  Future<List<AdSet>> getAllUserAdset(int id) async {
    String token = await RestClient.getAuthToken();
    var data =
        await RestClient.procesGetRequest("user/findByUserById/$id", token);
    List<AdSet> adsets = [];

    if (data is Map && data.containsKey("adset")) {
      for (var item in data['adset']) {
        AdSet adset = AdSet.fromJson(item);
        adsets.add(adset);
      }
    }
    _facebook.adsetLength = adsets;
    return adsets;
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'nom': this.name,
      'optimization_goal': this.optimization_goal,
      'billing_event': this.billing_event,
      'targeting': this.targeting,
      'bid_amount': this.bid_amount,
      'type_budget': this.type_budget,
      'statut': 'PAUSED',
      'budget': this.budget,
      'start_time': this.start_time,
      'end_time': this.end_time
    };
  }
}
