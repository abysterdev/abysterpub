import 'dart:convert';
import 'dart:io';
import 'package:abysterpub/restClient.dart';
import 'package:abysterpub/states/facebookStates.dart';
import 'package:dio/dio.dart' as dio;
import 'dart:async';

const AD_ACCOUNT_ID = '533844327313364';
const ACCESS_TOKEN =
    'EAAyqNy9FbhABAFT48LvIGeRC8Mpl4PBNk3amwTo6seLi8IgVlCTA6AIlO7c1rz5cz75fTcQlvQlqJczseYLp1XYhy8mE7giZAlledqaIp93emghxAl9CjyfsyUJmfeyWFN1y4Vc7zKq8lURzN0jU1GmaFHRp8WDHZCUGYPrsnfN6YydXL9Oc1Ha7UnyT4ZD';
FacebookStates _facebook = new FacebookStates();

// ignore: missing_return

// params = (
//     ('type', 'adinterest'),
//     ('q', 'movie'),
//     ('access_token', 'ACCESS_TOKEN'),
// )

// response = requests.get('https://graph.facebook.com/v8.0/search', params=params)

class Campaign {
  int id;
  String name;
  String objectif;
  String status;

  Campaign({
    this.id,
    this.name,
    this.objectif,
    this.status,
  });

  factory Campaign.fromJson(Map<String, dynamic> json) {
    return Campaign(
      name: json['name'],
      objectif: json['objectif'],
      status: json['status'],
      id: json['id_campagne'],
    );
  }
  FacebookStates _facebook;
  Future<dynamic> createCampaign(
      String name, String objectif, String status, String categories) async {
    try {
      final formData = dio.FormData.fromMap({
        'name': name,
        'objectif': objectif,
        'status': status,
        'special_ad_categories': categories,
        'access_token': ACCESS_TOKEN
      });
      print("ramses${name},${objectif}");
      final response1 = await dio.Dio().post(
        'https://graph.facebook.com/v9.0/act_533844327313364/campaigns',
        data: formData,
      );
      Map<String, dynamic> params = {
        "type": 'adinterest',
        'q': 'movie',
        'access_token': ACCESS_TOKEN,
      };

      // final response2 = await dio.Dio()
      //     .get('https://graph.facebook.com/v8.0/search', queryParameters: params);
      if (response1.statusCode == HttpStatus.ok) {
        // _facebook.isCampaignLoading = true;
        print(response1.data);
        return response1.data['id'];
      } else {
        // _facebook.isCampaignLoading = false;
        return Future.value("error");
      }
      // if (response2.statusCode == HttpStatus.created) {
      //   print("success");
      //   print(response2.data);
      // }

      //else {
      //   throw Exception('Failed to create Campaign.');
      //}
    } on dio.DioError catch (e) {
      print(e.response.data);
      // print(e.response.headers);
      // print(e.response.request);
    }
  }

  Future<dynamic> saveCampaign() async {
    return RestClient.getAuthToken().then((String token) {
      return RestClient.procesPostRequest("/campaign/add", this.toJson(), token)
          .then((value) {
        if (value != 200) {
          return Future.value("error");
        } else {
          return Future.value("success");
        }
      });
    });
  }

  Future<List<Campaign>> getAllUserCampaigns(int id) async {
    String token = await RestClient.getAuthToken();
    var data =
        await RestClient.procesGetRequest("user/findByUserById/$id", token);
    List<Campaign> campaigns = [];

    if (data is Map && data.containsKey("campagne")) {
      for (var item in data['campagne']) {
        Campaign campaign = Campaign.fromJson(item);
        campaigns.add(campaign);
      }
    }
    _facebook.campaignLength = campaigns;
    return campaigns;
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'nom': this.name,
      'objectif': this.objectif,
      'statut': 'PAUSED'
    };
  }
}
