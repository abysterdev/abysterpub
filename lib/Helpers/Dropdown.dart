class Company {
  int id;
  String name;

  Company(this.id, this.name);

  static List<Company> getCompanies() {
    return <Company>[
      Company(1, 'Admin'),
      Company(2, 'Agent'),
    ];
  }
}

class Facturation {
  int id;
  String name;
  Facturation(this.id, this.name);
  static List<Facturation> getFacturation() {
    return <Facturation>[
      Facturation(1, 'clicks'),
      Facturation(2, 'impressions'),
    ];
  }
}

class Optimization {
  int id;
  String name;
  Optimization(this.id, this.name);

  static List<Optimization> getOptimization() {
    return <Optimization>[
      Optimization(1, "REACH"),
    ];
  }
}

class Budget {
  int id;
  String name;
  Budget(this.id, this.name);
  static List<Budget> getBudget() {
    return <Budget>[Budget(1, 'journalier'), Budget(2, 'total')];
  }
}

class Objectif {
  int id;
  String name;
  Objectif(this.id, this.name);

  static List<Objectif> getObjectifs() {
    return <Objectif>[
      Objectif(1, 'trafic'),
      Objectif(2, 'Notoriété de la marque'),
      Objectif(3, 'couverture'),
      Objectif(4, 'interAction'),
      Objectif(5, 'installation d\'applications'),
      Objectif(6, 'vues de videos'),
      Objectif(7, 'generation de prospects'),
      Objectif(8, 'messages'),
      Objectif(9, 'conversions'),
      Objectif(10, 'vente catalogues'),
      Objectif(11, 'trafic en point de vente'),
    ];
  }
}

class Encheres {
  int id;
  String name;
  Encheres(this.id, this.name);
  static List<Encheres> getEncheres() {
    return <Encheres>[
      Encheres(1, 'automatique'),
      Encheres(2, 'manuelle'),
    ];
  }
}

class Targets {
  int id;
  String name;
  Targets(this.id, this.name);
  static List<Targets> getTargets() {
    return <Targets>[
      Targets(1, 'intérêts'),
      Targets(2, 'comportements'),
      Targets(3, 'données démographiques'),
    ];
  }
}

class Action2 {
  int id;
  String name;
  Action2(this.id, this.name);
  static List<Action2> getAction2() {
    return <Action2>[
      Action2(1, 'jouer à ce jeu'),
      Action2(2, 'réserver'),
      Action2(3, 'acheter'),
      Action2(4, 'télécharger'),
      Action2(5, 'profiter de l\'offre'),
      Action2(6, 'installer'),
      Action2(7, 'En savoir plus'),
      Action2(8, 'S\'abonner'),
    ];
  }
}
