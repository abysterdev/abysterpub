import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:abysterpub/bottom_navy_bar.dart';

class MMpataBottomNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavyBar(
      showElevation: true, // use this to remove appBar's elevation
      onItemSelected: (index) {
        print(index);
      },
      items: [
        BottomNavyBarItem(
          icon: Icon(Icons.home),
          title: Text('Accueil'),
          activeColor: Colors.teal,
          inactiveColor: Colors.grey,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.ondemand_video),
          title: Text('Mes publicites'),
          activeColor: Colors.teal,
          inactiveColor: Colors.grey,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.attach_money),
          title: Text('Paiement'),
          activeColor: Colors.teal,
          inactiveColor: Colors.grey,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.person_outline),
          title: Text('Profile'),
          activeColor: Colors.teal,
          inactiveColor: Colors.grey,
        ),
      ],
    );
  }
}
