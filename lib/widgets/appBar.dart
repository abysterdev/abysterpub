import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Appbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new GradientAppBar(
      title: Text("AbysterPub"),
      backgroundColorStart: Colors.blue,
      backgroundColorEnd: Colors.teal,
    );
  }
}
