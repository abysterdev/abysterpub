import 'dart:convert';

import 'package:abysterpub/Screens/SignInScreen.dart';
import 'package:abysterpub/models/utilisateur.dart';
import 'package:abysterpub/restClient.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:abysterpub/Screens/Welcome.dart';

class SideBar extends StatefulWidget {
  SideBarState createState() => SideBarState();
}

class SideBarState extends State<SideBar> {
  Utilisateur user;
  @override
  void initState() {
    super.initState();
    _loadUserData();
  }

  Future<void> _loadUserData() async {
    await Utilisateur.loadUserDataFromStorage().then((user1) {
      if (user1 == null) {
        // Navigator.of(context).pushAndRemoveUntil(
        //     MaterialPageRoute(
        //         builder: (BuildContext context) => SignInScreen()),
        //     (Route<dynamic> route) => false);
      } else {
        setState(() {
          this.user = user1;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        height: MediaQuery.of(context).size.height,
        child: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: new Text(user.email,
                    style: new TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0)),
                accountEmail: new Text(
                  user.role,
                  style: new TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
                currentAccountPicture: Icon(Icons.account_circle, size: 80),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
              ),
              ListTile(
                onTap: () {
                  Navigator.pushNamed(context, 'Welcome');
                },
                leading: Icon(Icons.home, size: 40),
                title: Text('Accueil', style: TextStyle(fontSize: 18)),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
              ),
              ListTile(
                onTap: () {
                  Navigator.pushNamed(context, 'pubDetails');
                },
                leading: Icon(Icons.cast_connected, size: 40),
                title: Text('Mes publicités', style: TextStyle(fontSize: 18)),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
              ),
              ListTile(
                onTap: () {
                  Navigator.pushNamed(context, 'notifications');
                },
                leading: Icon(Icons.message, size: 40),
                title: Text('Notifications', style: TextStyle(fontSize: 18)),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
              ),
              ListTile(
                onTap: () {
                  Navigator.pushNamed(context, 'profile');
                },
                leading: Icon(Icons.person_outline, size: 40),
                title: Text('Profil', style: TextStyle(fontSize: 18)),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
              ),
              ListTile(
                leading: Icon(Ionicons.md_log_out, size: 40),
                title: Text('Déconnexion', style: TextStyle(fontSize: 18)),
                onTap: () async {
                  await RestClient.deleteStringFromStorage('isLoggedIn');
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) => SignInScreen()),
                      (Route<dynamic> route) => false);
                },
              ),
            ],
          ),
        ));
  }
}
