import 'package:flutter/cupertino.dart';

class CustomPackageState extends ChangeNotifier {
  bool _isTextLoaded = false;
  bool _isLinkLoaded = false;
  bool _isImageLoaded = false;
  bool _isDescriptionLoaded = false;
  bool _isCTALoaded = false;
  bool _isTitleLoaded = false;
  bool get isTextLoaded => _isTextLoaded;
  bool get isImageLoaded => _isImageLoaded;
  bool get isDescriptionLoaded => _isDescriptionLoaded;
  bool get isCTALoaded => _isCTALoaded;
  bool get isTitleLoaded => _isTitleLoaded;
  bool get isLinkLoaded => _isLinkLoaded;

  set isLinkLoaded(bool val) {
    _isLinkLoaded = val;
    notifyListeners();
  }

  set isTextLoaded(bool val) {
    _isTextLoaded = val;
    notifyListeners();
  }

  set isImageLoaded(bool val) {
    _isImageLoaded = val;
    notifyListeners();
  }

  set isDescriptionLoaded(bool val) {
    _isDescriptionLoaded = val;
    notifyListeners();
  }

  set isCTAloaded(bool val) {
    _isCTALoaded = val;
    notifyListeners();
  }

  set isTitleLoaded(bool val) {
    _isTitleLoaded = val;
    notifyListeners();
  }

  void loadLink() {
    isLinkLoaded = !isLinkLoaded;
  }

  void loadText() {
    isTextLoaded = !isTextLoaded;
  }

  void loadCTA() {
    isCTAloaded = !isCTALoaded;
  }

  void loadDescription() {
    isDescriptionLoaded = !isDescriptionLoaded;
  }

  void loadTitle() {
    isTitleLoaded = !isDescriptionLoaded;
  }

  void loadImage() {
    isImageLoaded = !isImageLoaded;
  }
}
