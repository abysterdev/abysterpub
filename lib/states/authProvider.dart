import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class AuthProvider extends ChangeNotifier {
  bool _isloggedIn = false;
  bool _isRegistered = false;
  bool _isOTPsent = false;
  bool _isPasswordChanged = false;
  bool isLoginPageLoadingState = false;
  bool _isRegisterPageLoading = false;
  bool _isOTPPageLoading = false;
  bool _isChangePasswordLoading = false;
  bool get isLoginPageLoading => isLoginPageLoadingState;
  bool get isRegisterPageLoading => _isRegisterPageLoading;
  bool get isOTPPageLoading => _isOTPPageLoading;
  bool get isChangePasswordLoading => _isChangePasswordLoading;
  bool get isLoggedIn => _isloggedIn;
  bool get isRegistered => _isRegistered;
  bool get isOTPsent => _isOTPsent;
  bool get isPasswordChanged => _isPasswordChanged;

  set isLoginPageLoading(bool val) {
    isLoginPageLoadingState = val;
    notifyListeners();
  }

  set isRegisterPageLoading(bool val) {
    _isRegisterPageLoading = val;
    notifyListeners();
  }

  set isOTPPageLoading(bool val) {
    _isOTPPageLoading = val;
    notifyListeners();
  }

  set isChangePasswordLoading(bool val) {
    _isChangePasswordLoading = val;
    notifyListeners();
  }

  set isRegistered(bool val) {
    _isRegistered = val;
    notifyListeners();
  }

  set isLoggedIn(bool val) {
    _isloggedIn = val;
    notifyListeners();
  }

  set isOTPsent(bool val) {
    _isOTPsent = val;
    notifyListeners();
  }

  set isPasswordChanged(bool val) {
    _isPasswordChanged = val;
    notifyListeners();
  }

  // ignore: missing_return

  // void isNotLoggedIn() {
  //   _isloggedIn = false;
  //   notifyListeners();
  // }

  // void isLoggedIn() {
  //   _isloggedIn = true;
  //   notifyListeners();
  // }

  // void isRegisteredState() {
  //   _isRegistered = true;
  //   notifyListeners();
  // }

  // void isNotRegisteredState() {
  //   _isRegistered = false;
  //   notifyListeners();
  // }

}
