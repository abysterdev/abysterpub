import 'package:abysterpub/models/adset.dart';
import 'package:abysterpub/models/campagne.dart';
import 'package:abysterpub/models/publicite.dart';
import 'package:flutter/material.dart';

class FacebookStates extends ChangeNotifier {
  bool _isCampaign = false;
  bool _isAdset = false;
  bool _isAds = false;
  List<Campaign> _campaignLength;
  List<AdSet> _adsetLength;
  List<Ads> _ads;
  bool get isCampaignLoading => _isCampaign;
  bool get isAdsetLoading => _isAdset;
  bool get isAdLoading => _isAds;
  List<Campaign> get campaignLength => _campaignLength;
  List<AdSet> get adsetLength => _adsetLength;
  List<Ads> get adsLength => _ads;

  set adsLength(List<Ads> val) {
    _ads = val;
    notifyListeners();
  }

  set adsetLength(List<AdSet> val) {
    _adsetLength = val;
    notifyListeners();
  }

  set campaignLength(List<Campaign> val) {
    _campaignLength = val;
    notifyListeners();
  }

  set isCampaignLoading(bool val) {
    _isCampaign = val;
    notifyListeners();
  }

  set isAdsetLoading(bool val) {
    _isAdset = val;
    notifyListeners();
  }

  set isAdLoading(bool val) {
    _isAds = val;
    notifyListeners();
  }
}
