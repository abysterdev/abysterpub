import 'package:abysterpub/Screens/SignInScreen.dart';
import 'package:abysterpub/models/utilisateur.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => new _ProfileState();
}

class _ProfileState extends State<Profile> {
  Utilisateur user;
  @override
  void initState() {
    super.initState();
    _loadUserData();
  }

  Future<void> _loadUserData() async {
    await Utilisateur.loadUserDataFromStorage().then((user1) {
      if (user1 == null) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => SignInScreen()),
            (Route<dynamic> route) => false);
      } else {
        setState(() {
          this.user = user1;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var hauteurEcran = MediaQuery.of(context).size.height;
    var largeurEcran = MediaQuery.of(context).size.width;
    return new Scaffold(
        body: SingleChildScrollView(
            child: Center(
                child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [Colors.blue, Colors.teal])),
                    child: Column(children: <Widget>[
                      IconButton(
                        padding: EdgeInsets.fromLTRB(0, 150, 0, 15),
                        icon: Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                        onPressed: null,
                        iconSize: 150,
                      ),
                      Expanded(
                          child: Container(
                              child: Card(
                                  margin: EdgeInsets.fromLTRB(
                                      largeurEcran / 22,
                                      hauteurEcran / 29,
                                      largeurEcran / 22,
                                      hauteurEcran / 5),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15)),
                                  elevation: 4.0,
                                  color: Colors.white,
                                  child: ListView(
                                      shrinkWrap: true,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Row(
                                            children: <Widget>[
                                              IconButton(
                                                  icon: Icon(Icons.email),
                                                  onPressed: null),
                                              Expanded(
                                                  child: TextField(
                                                readOnly: true,
                                                decoration: InputDecoration(
                                                  enabled: false,
                                                  labelText: user.email,
                                                ),
                                              ))
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Row(
                                            children: <Widget>[
                                              IconButton(
                                                  icon: Icon(Icons.person),
                                                  onPressed: null),
                                              Expanded(
                                                  child: TextField(
                                                decoration: InputDecoration(
                                                    enabled: false,
                                                    labelText: user.role),
                                              ))
                                            ],
                                          ),
                                        ),
                                      ]))))
                    ])))));
  }
}
