import 'package:flutter/material.dart';

class Valider_Payment extends StatefulWidget {
  @override
  _Valider_PaymentState createState() => _Valider_PaymentState();
}

class _Valider_PaymentState extends State<Valider_Payment> {
  createAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            contentPadding: EdgeInsets.all(15.0),
            title: Text("Paiement effectué avec succès",
                style: TextStyle(color: Colors.teal)),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [Colors.blue, Colors.teal])),
            child: Column(
              children: <Widget>[
                Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  margin: EdgeInsets.fromLTRB(35, 250, 35, 30),
                  elevation: 4.0,
                  color: Colors.white,
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Row(
                          children: <Widget>[
                            IconButton(
                                icon: Icon(Icons.person), onPressed: null),
                            Expanded(
                                child: Container(
                                    margin:
                                        EdgeInsets.only(right: 20, left: 10),
                                    child: TextField(
                                      decoration:
                                          InputDecoration(hintText: 'code'),
                                    )))
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Container(
                            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 6.0),
                            child: RaisedButton(
                              padding: EdgeInsets.only(right: 1, left: 1),
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(20.0),
                              ),
                              onPressed: () {
                                createAlertDialog(context);
                              },
                              color: Colors.blue,
                              child: Text(
                                'Valider paiement',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
