import 'package:abysterpub/Screens/SignInScreen.dart';
import 'package:abysterpub/models/campagne.dart';
import 'package:abysterpub/models/publicite.dart';
import 'package:abysterpub/models/utilisateur.dart';
import 'package:abysterpub/restClient.dart';
import 'package:abysterpub/states/facebookStates.dart';
import 'package:flutter/material.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:abysterpub/models/adset.dart';

class PubDetails extends StatefulWidget {
  @override
  _PubDetailstate createState() => _PubDetailstate();
}

class _PubDetailstate extends State<PubDetails> {
  Future<List<Campaign>> campaigns;
  Future<List<AdSet>> adsets;
  Future<List<Ads>> ads;
  Utilisateur user;
  Campaign _campaign;
  AdSet _adset;
  Ads _ads;
  FacebookStates _facebook;
  Future<void> _loadUserData() async {
    await Utilisateur.loadUserDataFromStorage().then((user1) {
      if (user1 == null) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => SignInScreen()),
            (Route<dynamic> route) => false);
      } else {
        setState(() {
          this.user = user1;
        });
      }
    });
  }

  void initState() {
    super.initState();
    _loadUserData();
    campaigns = _campaign.getAllUserCampaigns(user.id_utilisateur);
    adsets = _adset.getAllUserAdset(user.id_utilisateur);
    ads = _ads.getAllUserAds(user.id_utilisateur);
  }

  Widget build(BuildContext context) {
    return Scaffold(
        drawer: SideBar(),
        appBar: GradientAppBar(
          title: Text("AbysterPub"),
          backgroundColorStart: Colors.blue,
          backgroundColorEnd: Colors.teal,
          bottom: TabBar(
            labelStyle: TextStyle(fontSize: 18),
            tabs: [
              Tab(text: "Campagnes"),
              Tab(text: "Groupes pubs"),
              Tab(text: "Publicités"),
            ],
          ),
        ),
        body: TabBarView(children: [
          FutureBuilder(
              future: campaigns,
              builder: (context, AsyncSnapshot<List<Campaign>> snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      itemCount: _facebook.campaignLength.length,
                      itemBuilder: (context, index) {
                        Campaign camp = _facebook.campaignLength[index];
                        final ThemeData theme = Theme.of(context);
                        return InkWell(
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              children: <Widget>[
                                Row(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          18.0, 0.0, 0.0, 0.0)),
                                  Text(
                                    camp.name,
                                    style: theme.textTheme.title,
                                    maxLines: 1,
                                    textAlign: TextAlign.left,
                                  ),
                                ]),
                                Row(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          18.0, 0.0, 0.0, 0.0)),
                                  Text(''),
                                ]),
                                Row(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          18.0, 0.0, 0.0, 0.0)),
                                  Expanded(
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                        Text(
                                          camp.objectif,
                                          style: TextStyle(
                                              fontStyle: FontStyle.italic),
                                          maxLines: 3,
                                        ),
                                      ])),
                                ]),

                                //SizedBox(height: 13.0),
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 18.0),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          child: Text(
                                            "statut:${camp.status}",
                                            style: theme.textTheme.subtitle,
                                            maxLines: 3,
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            InkWell(
                                              onTap: () {},
                                              child: RichText(
                                                text: TextSpan(
                                                    text:
                                                        'Créer un groupe de publicité ',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                    children: [
                                                      TextSpan(
                                                        text: '?',
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      )
                                                    ]),
                                              ),
                                            ),
                                          ],
                                        )
                                      ]),
                                ),
                                Divider(
                                  thickness: 0.3,
                                  color: Colors.black,
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                } else if (snapshot.hasError) {
                  return Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: 60,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                            child:
                                Text("Erreur lors du chargement des données."),
                          )
                        ]),
                  );
                } else {
                  return Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            child: CircularProgressIndicator(),
                            width: 60,
                            height: 60,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                            child: Text("Chargement en cours..."),
                          )
                        ]),
                  );
                }
              }),
          FutureBuilder(
              future: adsets,
              builder: (context, AsyncSnapshot<List<AdSet>> snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      itemCount: _facebook.adsetLength.length,
                      itemBuilder: (context, index) {
                        AdSet camp = _facebook.adsetLength[index];
                        final ThemeData theme = Theme.of(context);
                        return InkWell(
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              children: <Widget>[
                                Row(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          18.0, 0.0, 0.0, 0.0)),
                                  Text(
                                    camp.name,
                                    style: theme.textTheme.title,
                                    maxLines: 1,
                                    textAlign: TextAlign.left,
                                  ),
                                ]),
                                Row(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          18.0, 0.0, 0.0, 0.0)),
                                  Text(''),
                                ]),
                                Row(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          18.0, 0.0, 0.0, 0.0)),
                                  Expanded(
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                        Text(
                                          camp.budget,
                                          style: TextStyle(
                                              fontStyle: FontStyle.italic),
                                          maxLines: 3,
                                        ),
                                      ])),
                                ]),

                                //SizedBox(height: 13.0),
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 18.0),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          child: Text(
                                            "statut:${camp.status}",
                                            style: theme.textTheme.subtitle,
                                            maxLines: 3,
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            InkWell(
                                              onTap: () {},
                                              child: RichText(
                                                text: TextSpan(
                                                    text:
                                                        'Créer une publicité ',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                    children: [
                                                      TextSpan(
                                                        text: '?',
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      )
                                                    ]),
                                              ),
                                            ),
                                          ],
                                        )
                                      ]),
                                ),
                                Divider(
                                  thickness: 0.3,
                                  color: Colors.black,
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                } else if (snapshot.hasError) {
                  return Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: 60,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                            child:
                                Text("Erreur lors du chargement des données."),
                          )
                        ]),
                  );
                } else {
                  return Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            child: CircularProgressIndicator(),
                            width: 60,
                            height: 60,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                            child: Text("Chargement en cours..."),
                          )
                        ]),
                  );
                }
              }),
          FutureBuilder(
              future: campaigns,
              builder: (context, AsyncSnapshot<List<Campaign>> snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      itemCount: _facebook.campaignLength.length,
                      itemBuilder: (context, index) {
                        Campaign camp = _facebook.campaignLength[index];
                        final ThemeData theme = Theme.of(context);
                        return InkWell(
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              children: <Widget>[
                                Row(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          18.0, 0.0, 0.0, 0.0)),
                                  Text(
                                    camp.name,
                                    style: theme.textTheme.title,
                                    maxLines: 1,
                                    textAlign: TextAlign.left,
                                  ),
                                ]),
                                Row(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          18.0, 0.0, 0.0, 0.0)),
                                  Text(''),
                                ]),
                                Row(children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          18.0, 0.0, 0.0, 0.0)),
                                  Expanded(
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                        Text(
                                          camp.objectif,
                                          style: TextStyle(
                                              fontStyle: FontStyle.italic),
                                          maxLines: 3,
                                        ),
                                      ])),
                                ]),

                                //SizedBox(height: 13.0),
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 18.0),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          child: Text(
                                            "statut:${camp.status}",
                                            style: theme.textTheme.subtitle,
                                            maxLines: 3,
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            InkWell(
                                              onTap: () {},
                                              child: RichText(
                                                text: TextSpan(
                                                    text:
                                                        'Créer un groupe de publicité ',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                    children: [
                                                      TextSpan(
                                                        text: '?',
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      )
                                                    ]),
                                              ),
                                            ),
                                          ],
                                        )
                                      ]),
                                ),
                                Divider(
                                  thickness: 0.3,
                                  color: Colors.black,
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                } else if (snapshot.hasError) {
                  return Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: 60,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                            child:
                                Text("Erreur lors du chargement des données."),
                          )
                        ]),
                  );
                } else {
                  return Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            child: CircularProgressIndicator(),
                            width: 60,
                            height: 60,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                            child: Text("Chargement en cours..."),
                          )
                        ]),
                  );
                }
              }),
        ])
        // Changed code
        );
  }
}
