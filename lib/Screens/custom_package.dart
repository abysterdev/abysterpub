import 'package:abysterpub/states/customPackageState.dart';
import 'package:date_field/date_field.dart';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:abysterpub/Helpers/Dropdown.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter_flexible_toast/flutter_flexible_toast.dart';

class Custom_Package extends StatefulWidget {
  // final image;
  // Custom_Package({
  //   this.image,
  // });
  @override
  _Custom_PackageState createState() => _Custom_PackageState();
}

class _Custom_PackageState extends State<Custom_Package> {
  CustomPackageState _customPackageState = CustomPackageState();
  DateTime selectedDate1;
  DateTime selectedDate2;
  Set<String> lastFiles;
  String _fileString = 'String';
  String _fileName;
  String _path;
  Map<String, String> _paths;
  List<String> _extension = ['jpg', 'png', 'jpeg', 'mp4'];
  bool _loadingPath = false;
  bool _multiPick = false;
  bool _hasValidMime = false;
  FileType _pickingType;
  TextEditingController _namecontroller = new TextEditingController();
  TextEditingController _contentcontroller = new TextEditingController();
  TextEditingController _titlecontroller = new TextEditingController();
  TextEditingController _ctacontroller = new TextEditingController();
  TextEditingController _descriptioncontroller = new TextEditingController();
  TextEditingController _linkcontroller = new TextEditingController();

  @override
  void dispose() {
    _namecontroller.dispose();
    super.dispose();
  }

  void showColoredToast() {
    FlutterFlexibleToast.showToast(
        message: "Short red Success Toast",
        toastLength: Toast.LENGTH_SHORT,
        backgroundColor: Colors.red,
        icon: ICON.SUCCESS,
        fontSize: 16,
        imageSize: 35,
        textColor: Colors.white);
  }

  List<Targets> _targets = Targets.getTargets();
  List<DropdownMenuItem<Targets>> _dropdownMenuTargetItems;
  Targets _selectedTargets;

  List<Optimization> _optimization = Optimization.getOptimization();
  List<DropdownMenuItem<Optimization>> _dropdownMenuOptItems;
  Optimization _selectedOptimization;

  List<Action2> _action = Action2.getAction2();
  List<DropdownMenuItem<Action2>> _dropdownMenuActionItems;
  Action2 _selectedActions;

  List<Encheres> _encheres = Encheres.getEncheres();
  List<DropdownMenuItem<Encheres>> _dropdownMenuEncheresItems;
  Encheres _selectedEncheres;

  List<Facturation> _facturation = Facturation.getFacturation();
  List<DropdownMenuItem<Facturation>> _dropdownMenuFacturationItems;
  Facturation _selectedFacturation;

  List<Objectif> _objectifs = Objectif.getObjectifs();
  List<DropdownMenuItem<Objectif>> _dropdownMenuItems;
  Objectif _selectedObjectifs;

  void initState() {
    FilePickerCross.listInternalFiles()
        .then((value) => setState(() => lastFiles = value.toSet()));
    _dropdownMenuFacturationItems =
        buildDropdownFacturationMenuItems(_facturation);
    _dropdownMenuOptItems = buildDropdownOptimizationMenuItems(_optimization);
    _dropdownMenuActionItems = buildDropdownActionMenuItems(_action);
    _dropdownMenuEncheresItems = buildDropdownEncheresMenuItems(_encheres);
    _dropdownMenuTargetItems = buildDropdownTargetsMenuItems(_targets);
    _dropdownMenuItems = buildDropdownObjectifMenuItems(_objectifs);
    _selectedObjectifs = null;
    _selectedTargets = null;
    _selectedEncheres = null;
    _selectedOptimization = null;
    _selectedActions = null;
    _selectedFacturation = null;
    super.initState();
  }

  List<DropdownMenuItem<Objectif>> buildDropdownObjectifMenuItems(
      List objectifs) {
    List<DropdownMenuItem<Objectif>> items = List();
    for (Objectif objectif in objectifs) {
      items.add(
        DropdownMenuItem(
          value: objectif,
          child: Text(objectif.name),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<Optimization>> buildDropdownOptimizationMenuItems(
      List optimizations) {
    List<DropdownMenuItem<Optimization>> items = List();
    for (Optimization optimization in optimizations) {
      items.add(DropdownMenuItem(
          value: optimization, child: Text(optimization.name)));
    }
    return items;
  }

  List<DropdownMenuItem<Action2>> buildDropdownActionMenuItems(List actions) {
    List<DropdownMenuItem<Action2>> items = List();
    for (Action2 action in actions) {
      items.add(
        DropdownMenuItem(
          value: action,
          child: Text(action.name),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<Facturation>> buildDropdownFacturationMenuItems(
      List facturations) {
    List<DropdownMenuItem<Facturation>> items = List();
    for (Facturation facturation in facturations) {
      items.add(
        DropdownMenuItem(
          value: facturation,
          child: Text(facturation.name),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<Encheres>> buildDropdownEncheresMenuItems(
      List encheres) {
    List<DropdownMenuItem<Encheres>> items = List();
    for (Encheres enchere in encheres) {
      items.add(
        DropdownMenuItem(
          value: enchere,
          child: Text(enchere.name),
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<Targets>> buildDropdownTargetsMenuItems(List targets) {
    List<DropdownMenuItem<Targets>> items = List();
    for (Targets target in targets) {
      items.add(
        DropdownMenuItem(
          value: target,
          child: Text(target.name),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(Objectif selectedObjectifs) {
    setState(() {
      _selectedObjectifs = selectedObjectifs;
    });
  }

  onChangeDropdownItem6(Optimization selectedOptimization) {
    setState(() {
      _selectedOptimization = selectedOptimization;
    });
  }

  onChangeDropdownItem5(Action2 selectedActions) {
    setState(() {
      _selectedActions = selectedActions;
    });
  }

  onChangeDropdownItem4(Facturation selectedFacturation) {
    setState(() {
      _selectedFacturation = selectedFacturation;
    });
  }

  onChangeDropdownItem2(Targets selectedTargets) {
    setState(() {
      _selectedTargets = selectedTargets;
    });
  }

  onChangeDropdownItem3(Encheres selectedEncheres) {
    setState(() {
      _selectedEncheres = selectedEncheres;
    });
  }

  FilePickerCross filePickerCross;
  setFilePicker(FilePickerCross filePicker) => setState(() {
        filePickerCross = filePicker;
        filePickerCross.saveToPath(path: filePickerCross.fileName);
        // FilePickerCross.quota().then((value) {
        //   print(value);
        //   setState(() => quota = value);
        // });
        lastFiles.add(filePickerCross.fileName);
        try {
          _fileString = filePickerCross.toString();
        } catch (e) {
          _fileString = 'Not a text file. Showing base64.\n\n' +
              filePickerCross.toBase64();
        }
      });
  void _selectFile() {
    FilePickerCross.importFromStorage()
        .then((filePicker) => setFilePicker(filePicker));
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      drawer: SideBar(),
      appBar: GradientAppBar(
        title: Text("AbysterPub"),
        backgroundColorStart: Colors.blue,
        backgroundColorEnd: Colors.teal,
      ),
      body: Center(
          child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Colors.blue, Colors.teal])),
        child: Column(
          children: <Widget>[
            Expanded(
                child: Card(
              margin:
                  EdgeInsets.fromLTRB(width / 20, height / 28, width / 20, 10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              elevation: 4.0,
              color: Colors.white,
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  ListTile(
                    title: (_customPackageState.isTextLoaded)
                        ? Text(
                            _contentcontroller.text,
                            maxLines: null,
                          )
                        : Text(
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor'),
                  ),
                  Container(
                      padding: new EdgeInsets.all(18.0),
                      child: (_customPackageState.isImageLoaded)
                          ? Image.memory(filePickerCross.toUint8List())
                          : Image.asset("asset/img/img1.png")),
                  Row(
                    children: [
                      (_customPackageState.isLinkLoaded)
                          ? Text(_linkcontroller.text)
                          : Text("https://abyster.com"),
                    ],
                  ),
                  Container(
                    padding: new EdgeInsets.all(18.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            (_customPackageState.isTitleLoaded)
                                ? Text(
                                    _titlecontroller.text,
                                    maxLines: 1,
                                  )
                                : Text("Title")
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(right: 10.0),
                                child: Text(
                                  (_customPackageState.isDescriptionLoaded)
                                      ? _descriptioncontroller.text
                                      : "",
                                  maxLines: 1,
                                )),
                            Padding(
                              padding: EdgeInsets.only(left: 10.0),
                              child: RaisedButton(
                                onPressed: () => null,
                                child: new Text(
                                    (_customPackageState.isCTALoaded)
                                        ? _ctacontroller.text
                                        : "Partager"),
                                textColor: Colors.white,
                                color: Colors.blue,
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(20.0),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )),
            Expanded(
              child: Card(
                margin: EdgeInsets.fromLTRB(
                    width / 20, height / 28, width / 20, 10),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                elevation: 4.0,
                color: Colors.white,
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                          "Veuillez renseigner les informations suivantes"),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(right: 20, left: 10),
                                  child: TextFormField(
                                    controller: _namecontroller,
                                    decoration: InputDecoration(
                                        hintText: "Nom de la campagne"),
                                  )))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(right: 10, left: 8),
                              child: DropdownButton(
                                hint: Text("cible"),
                                isExpanded: true,
                                value: _selectedTargets,
                                items: _dropdownMenuTargetItems,
                                onChanged: onChangeDropdownItem2,
                              ),
                            ),
                          ),
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(
                                      right: 20, left: 10, bottom: 18),
                                  child: TextField(
                                    decoration: InputDecoration(hintText: ''),
                                  ))),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(right: 20, left: 10),
                            child: DropdownButton(
                              hint: new Text("objectif"),
                              isExpanded: true,
                              value: _selectedObjectifs,
                              items: _dropdownMenuItems,
                              onChanged: onChangeDropdownItem,
                            ),
                          )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(right: 20, left: 10),
                            child: DropdownButton(
                              hint: Text("facturation"),
                              isExpanded: true,
                              value: _selectedFacturation,
                              items: _dropdownMenuFacturationItems,
                              onChanged: onChangeDropdownItem4,
                            ),
                          )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(right: 20, left: 10),
                            child: DropdownButton(
                              hint: new Text("Encheres"),
                              isExpanded: true,
                              value: _selectedEncheres,
                              items: _dropdownMenuEncheresItems,
                              onChanged: onChangeDropdownItem3,
                            ),
                          )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(right: 20, left: 10),
                                  child: TextField(
                                    decoration: InputDecoration(
                                        hintText: "Nom du groupe de publicité"),
                                  )))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(right: 20, left: 10),
                            child: DropdownButton(
                              hint: Text("optimisation"),
                              isExpanded: true,
                              value: _selectedOptimization,
                              items: _dropdownMenuOptItems,
                              onChanged: onChangeDropdownItem6,
                            ),
                          )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(right: 20, left: 10),
                                  child: TextField(
                                    decoration: InputDecoration(
                                        enabled: false, labelText: 'budget'),
                                  ))),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(right: 20, left: 10),
                            child: DateTimeField(
                              firstDate: DateTime(2020),
                              lastDate: DateTime(2021),
                              label: 'date de debut',
                              mode: DateFieldPickerMode.dateAndTime,
                              selectedDate: selectedDate1,
                              onDateSelected: (DateTime date) {
                                setState(() {
                                  selectedDate1 = date;
                                });
                              },
                            ),
                          ))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(right: 20, left: 10),
                            child: DateTimeField(
                              firstDate: DateTime(2020),
                              lastDate: DateTime(2021),
                              label: 'date de fin',
                              mode: DateFieldPickerMode.dateAndTime,
                              selectedDate: selectedDate2,
                              onDateSelected: (DateTime date) {
                                setState(() {
                                  selectedDate2 = date;
                                });
                              },
                            ),
                          ))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(right: 20, left: 10),
                                  child: TextField(
                                    onEditingComplete:
                                        _customPackageState.loadTitle,
                                    controller: _titlecontroller,
                                    decoration: InputDecoration(
                                        hintText: "Titre de la page"),
                                  )))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(right: 20, left: 10),
                                  child: TextFormField(
                                    onEditingComplete:
                                        _customPackageState.loadDescription,
                                    controller: _descriptioncontroller,
                                    decoration: InputDecoration(
                                        hintText: "Description"),
                                  )))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(right: 20, left: 10),
                                  child: TextFormField(
                                    onEditingComplete:
                                        _customPackageState.loadText,
                                    maxLines: null,
                                    controller: _contentcontroller,
                                    decoration:
                                        InputDecoration(hintText: "Texte"),
                                  )))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(right: 20, left: 10),
                                  child: TextField(
                                    decoration: InputDecoration(
                                        hintText: 'lien de la page'),
                                  ))),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.only(right: 20, left: 10),
                            child: DropdownButton(
                              hint: Text("bouton appel à action"),
                              isExpanded: true,
                              value: _selectedActions,
                              items: _dropdownMenuActionItems,
                              onChanged: onChangeDropdownItem5,
                            ),
                          )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(right: 20, left: 10),
                                  child: TextField(
                                    decoration:
                                        InputDecoration(hintText: 'Texte'),
                                  ))),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                        initialValue: (filePickerCross != null)
                            ? '${filePickerCross.toBase64().toString()}'
                            : '',
                        readOnly: true,
                        style: TextStyle(color: Color(0xFF000000)),
                        // onSaved: (String value) {
                        //   _user.cni = (filePickerCross != null)
                        //       ? filePickerCross.toBase64().toString()
                        //       : '';
                        //   //print("valeur:$value");
                        // },
                        cursorColor: Color(0xFF9b9b9b),
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: (filePickerCross != null)
                              ? '${filePickerCross.fileName}'
                              : 'image ou vidéo',
                          hintStyle: TextStyle(
                              color: Color(0xFF9b9b9b),
                              fontSize: 15,
                              fontWeight: FontWeight.normal),
                          suffixIcon: IconButton(
                              icon: Icon(Icons.attach_file),
                              color: Colors.blue,
                              onPressed: () {
                                _selectFile();
                                _customPackageState.loadImage();
                              }),
                        ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(21, 10, 0.0, 10),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Container(
                              child: RaisedButton(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(20.0),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                color: Colors.blue,
                                child: Text(
                                  'Annuler',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(95, 10, 0.0, 10),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Container(
                              child: RaisedButton(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(20.0),
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, 'publish');
                                },
                                color: Colors.blue,
                                child: Text(
                                  'Valider',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
