import 'package:abysterpub/Helpers/dropdown_formfield.dart';
import 'package:abysterpub/Helpers/formValidator.dart';
import 'package:abysterpub/Screens/create_pub.dart';
import 'package:abysterpub/states/facebookStates.dart';
import 'package:abysterpub/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'dart:async';
import 'package:abysterpub/Helpers/Dropdown.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:date_field/date_field.dart';
import 'package:abysterpub/models/adset.dart';
import 'package:country_picker/country_picker.dart';
import 'package:fluttericon/typicons_icons.dart';
import 'package:provider/provider.dart';
import 'package:flutter_flexible_toast/flutter_flexible_toast.dart';

class CreateAdset extends StatefulWidget {
  dynamic id;
  CreateAdset(this.id);
  @override
  _CreateAdsetState createState() => _CreateAdsetState();
}

class _CreateAdsetState extends State<CreateAdset> {
  GlobalKey<FormState> _key = new GlobalKey();
  AdSet _adset;
  String _myoptimisation;
  String _myoptimisationResult;
  String _myfacturation;
  String _myfacturationResult;
  String _mytarget;
  String _myTarget;
  String _mytargetResult;
  String _myEnchere;
  String _myEnchereResult;
  String _myBudget;
  String _myBudgetResult;
  DateTime selectedDate1;
  DateTime selectedDate2;
  TextEditingController nameController = new TextEditingController();
  TextEditingController bidcontroller = new TextEditingController();
  TextEditingController budgetcontroller = new TextEditingController();
  Country country;
  String countryValue;
  String countryCode;
  String countrycode2;
  final formKey = new GlobalKey<FormState>();
  FacebookStates face;
  @override
  void initState() {
    super.initState();
    _myoptimisation = '';
    _mytarget = '';
    _myTarget = '';
    _myEnchere = '';
    _myBudget = '';
    _myfacturation = '';
    _mytargetResult = '';
    _myBudgetResult = '';
    _myEnchereResult = '';
    _myoptimisationResult = '';
    _myfacturationResult = '';
    selectedDate1 = DateTime.now();
    selectedDate2 = DateTime.now();
  }

  void showToast(String message, ICON icon) {
    FlutterFlexibleToast.showToast(
        message: message,
        toastLength: Toast.LENGTH_LONG,
        toastGravity: ToastGravity.BOTTOM,
        icon: icon,
        radius: 100,
        elevation: 10,
        imageSize: 35,
        textColor: Colors.white,
        backgroundColor: Colors.black,
        timeInSeconds: 2);
  }

  _saveForm() {
    var form = formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {
        _mytargetResult = _mytarget;
        _myoptimisationResult = _myoptimisation;
        _myEnchereResult = _myEnchere;
        _myfacturationResult = _myfacturation;
        _myBudgetResult = _myBudget;
      });
    }
  }

  void setcountryValue(String value) {
    setState(() {
      countryValue = value;
    });
  }

  void adseTreatment() async {
    if (_key.currentState.validate()) {
      _key.currentState.save();
      await _adset
          .createAdSet(
              nameController.text,
              _mytarget,
              _myfacturation,
              bidcontroller.text,
              _myBudget,
              budgetcontroller.text,
              countrycode2,
              _myTarget,
              selectedDate1,
              selectedDate2,
              'PAUSED',
              widget.id)
          .then((response) {
        if (response == "error") {
          face.isAdsetLoading = false;
          showToast(
              "Erreur lors de la création de l'ensemble de pub !", ICON.ERROR);
        } else {
          face.isAdsetLoading = false;
          showToast("Groupe de publicité crée avec succès !", ICON.SUCCESS);
          // _adset.saveAdset();
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Create_pub(response)),
          );
        }
      });
    }
  }

  void setcountryValue2(String value) {
    setState(() {
      countryCode = value;
    });
  }

  void setcountryValue3(String value) {
    setState(() {
      countrycode2 = value;
    });
  }
  // DateTime selectedDate = DateTime.now();
  // Future<void> _selectDate(BuildContext context) async {
  //   final DateTime picked = await showDatePicker(
  //       context: context,
  //       initialDate: selectedDate,
  //       firstDate: DateTime(2020),
  //       lastDate: DateTime(2050));
  //   if (picked != null && picked != selectedDate)
  //     setState(() {
  //       selectedDate = picked;
  //     });
  // }

  Widget build(BuildContext context) {
    var hauteurEcran = MediaQuery.of(context).size.height;
    var largeurEcran = MediaQuery.of(context).size.width;

    return ChangeNotifierProvider<FacebookStates>(
        create: (context) => FacebookStates(),
        child: Builder(builder: (context) {
          return Scaffold(
              backgroundColor: Colors.white,
              drawer: SideBar(),
              appBar: GradientAppBar(
                title: Text("AbysterPub"),
                backgroundColorStart: Colors.blue,
                backgroundColorEnd: Colors.teal,
              ),
              body:
                  Consumer<FacebookStates>(builder: (context, provider, child) {
                return AbysterPubLoader(
                    inAsyncCall: provider.isAdsetLoading,
                    child: Center(
                        child: Container(
                            height: MediaQuery.of(context).size.height,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topRight,
                                    end: Alignment.bottomLeft,
                                    colors: [Colors.blue, Colors.teal])),
                            child: Column(children: <Widget>[
                              Expanded(
                                  child: Container(
                                child: Card(
                                  margin: EdgeInsets.fromLTRB(
                                      largeurEcran / 39,
                                      hauteurEcran / 16,
                                      largeurEcran / 22,
                                      hauteurEcran / 16),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15)),
                                  elevation: 4.0,
                                  color: Colors.white,
                                  child: ListView(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20, left: 10),
                                                    child: TextFormField(
                                                      validator: FormValidator()
                                                          .validateOtherFields,
                                                      showCursor: true,
                                                      controller:
                                                          nameController,
                                                      decoration:
                                                          InputDecoration(
                                                              hintText: "nom"),
                                                    )))
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    right: 10, left: 8),
                                                child: DropDownFormField(
                                                  validator: FormValidator()
                                                      .validateOtherFields,
                                                  hintText: 'optimisation',
                                                  value: _mytarget,
                                                  onSaved: (value) {
                                                    setState(() {
                                                      _mytarget = value;
                                                    });
                                                  },
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _mytarget = value;
                                                    });
                                                  },
                                                  dataSource: [
                                                    {
                                                      "display":
                                                          "Lecture complète de spots ",
                                                      "value": "THRUPLAY",
                                                    },
                                                    {
                                                      "display":
                                                          "Like de pages",
                                                      "value": "PAGE_LIKES",
                                                    },
                                                    {
                                                      "display":
                                                          "Like de pages",
                                                      "value": "IMPRESSIONS",
                                                    },
                                                  ],
                                                  textField: 'display',
                                                  valueField: 'value',
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20,
                                                        left: 10,
                                                        bottom: 18,
                                                        top: 38),
                                                    child: TextField(
                                                      showCursor: true,
                                                      decoration:
                                                          InputDecoration(
                                                              hintText: ''),
                                                    ))),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                                child: Container(
                                              margin: EdgeInsets.only(
                                                  right: 20, left: 10),
                                              child: DropDownFormField(
                                                validator: FormValidator()
                                                    .validateOtherFields,
                                                hintText: 'facturation',
                                                value: _myfacturation,
                                                onSaved: (value) {
                                                  setState(() {
                                                    _myfacturation = value;
                                                  });
                                                },
                                                onChanged: (value) {
                                                  setState(() {
                                                    _myfacturation = value;
                                                  });
                                                },
                                                dataSource: [
                                                  {
                                                    "display": "Impressions",
                                                    "value": "IMPRESSIONS",
                                                  },
                                                  {
                                                    "display": "clics de liens",
                                                    "value": "LINK_CLICKS",
                                                  },
                                                  {
                                                    "display": "like de pages",
                                                    "value": "PAGE_LIKES",
                                                  },
                                                ],
                                                textField: 'display',
                                                valueField: 'value',
                                              ),
                                            )),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                                child: Container(
                                              margin: EdgeInsets.only(
                                                  right: 20, left: 10),
                                              child: new IgnorePointer(
                                                ignoring: false,
                                                child: TextFormField(
                                                  validator: FormValidator()
                                                      .validateOtherFields,
                                                  readOnly: true,
                                                  style: TextStyle(
                                                      color: Color(0xFF000000)),
                                                  cursorColor:
                                                      Color(0xFF9b9b9b),
                                                  keyboardType:
                                                      TextInputType.number,
                                                  decoration: InputDecoration(
                                                    prefixIcon: Icon(
                                                      Typicons.globe,
                                                      color: Colors.grey,
                                                    ),
                                                    hintText: (countryValue !=
                                                            '')
                                                        ? '${countryValue}'
                                                        : 'sélectionner pays',
                                                    hintStyle: TextStyle(
                                                        color:
                                                            Color(0xFF9b9b9b),
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.normal),
                                                    suffixIcon: IconButton(
                                                        icon: Icon(
                                                            Icons.add_circle),
                                                        color: Colors.blue,
                                                        onPressed: () {
                                                          showCountryPicker(
                                                            context: context,
                                                            showPhoneCode:
                                                                false,
                                                            onSelect: (Country
                                                                country) {
                                                              setcountryValue(
                                                                  country.name);
                                                              setcountryValue3(
                                                                  country
                                                                      .countryCode);
                                                            },
                                                          );
                                                        }),
                                                  ),
                                                ),
                                              ),
                                            )),
                                            Expanded(
                                                child: Container(
                                              margin: EdgeInsets.only(
                                                  right: 20, left: 10),
                                              child: new IgnorePointer(
                                                ignoring: false,
                                                child: DropDownFormField(
                                                  validator: FormValidator()
                                                      .validateOtherFields,
                                                  hintText: 'ciblage',
                                                  value: _myTarget,
                                                  onSaved: (value) {
                                                    setState(() {
                                                      _myTarget = value;
                                                    });
                                                  },
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _myTarget = value;
                                                    });
                                                  },
                                                  dataSource: [
                                                    {
                                                      "display": "Films",
                                                      "value": "Movies",
                                                    },
                                                    {
                                                      "display": "Jeux",
                                                      "value": "Games",
                                                    },
                                                  ],
                                                  textField: 'display',
                                                  valueField: 'value',
                                                ),
                                              ),
                                            )),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    right: 10, left: 8),
                                                child: DropDownFormField(
                                                  validator: FormValidator()
                                                      .validateOtherFields,
                                                  hintText: 'Enchères',
                                                  value: _myEnchere,
                                                  onSaved: (value) {
                                                    setState(() {
                                                      _myEnchere = value;
                                                    });
                                                  },
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _myEnchere = value;
                                                    });
                                                  },
                                                  dataSource: [
                                                    {
                                                      "display": "Automatique",
                                                      "value": "Automatique",
                                                    },
                                                    {
                                                      "display": "manuelle",
                                                      "value": "manuelle",
                                                    },
                                                  ],
                                                  textField: 'display',
                                                  valueField: 'value',
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20,
                                                        left: 10,
                                                        bottom: 18,
                                                        top: 38),
                                                    child: TextFormField(
                                                      validator: FormValidator()
                                                          .validateOtherFields,
                                                      showCursor: true,
                                                      controller: bidcontroller,
                                                      decoration:
                                                          InputDecoration(
                                                              hintText: ''),
                                                    ))),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    right: 10, left: 8),
                                                child: DropDownFormField(
                                                  validator: FormValidator()
                                                      .validateOtherFields,
                                                  hintText: 'Budget',
                                                  value: _myBudget,
                                                  onSaved: (value) {
                                                    setState(() {
                                                      _myBudget = value;
                                                    });
                                                    _adset.type_budget = value;
                                                  },
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _myBudget = value;
                                                    });
                                                  },
                                                  dataSource: [
                                                    {
                                                      "display": "daily_budget",
                                                      "value": "daily_budget",
                                                    },
                                                    {
                                                      "display":
                                                          "lifetime_budget",
                                                      "value":
                                                          "lifetime_budget",
                                                    },
                                                  ],
                                                  textField: 'display',
                                                  valueField: 'value',
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20,
                                                        left: 10,
                                                        bottom: 18,
                                                        top: 38),
                                                    child: TextFormField(
                                                      validator: FormValidator()
                                                          .validateOtherFields,
                                                      onSaved: (String value) {
                                                        _adset.budget = value;
                                                      },
                                                      controller:
                                                          budgetcontroller,
                                                      showCursor: true,
                                                      decoration:
                                                          InputDecoration(
                                                              hintText: ''),
                                                    ))),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                                child: Container(
                                              margin: EdgeInsets.only(
                                                  right: 20, left: 10),
                                              child: DateTimeField(
                                                firstDate: DateTime(2020),
                                                lastDate: DateTime(2021),
                                                label: 'date debut',
                                                mode: DateFieldPickerMode
                                                    .dateAndTime,
                                                selectedDate: selectedDate1,
                                                onDateSelected:
                                                    (DateTime date) {
                                                  setState(() {
                                                    selectedDate1 = date;
                                                  });
                                                },
                                              ),
                                            )),
                                            Expanded(
                                                child: Container(
                                              margin: EdgeInsets.only(
                                                  right: 20, left: 10),
                                              child: DateTimeField(
                                                firstDate: DateTime(2020),
                                                lastDate: DateTime(2021),
                                                label: 'date fin',
                                                mode: DateFieldPickerMode
                                                    .dateAndTime,
                                                selectedDate: selectedDate2,
                                                onDateSelected:
                                                    (DateTime date) {
                                                  setState(() {
                                                    selectedDate2 = date;
                                                  });
                                                },
                                              ),
                                            )),
                                          ],
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                25, 10, 0.0, 10),
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              child: Container(
                                                child: RaisedButton(
                                                  shape:
                                                      new RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(20.0),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  color: Colors.blue,
                                                  child: Text(
                                                    'Annuler',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                75, 10, 0.0, 10),
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              child: Container(
                                                child: RaisedButton(
                                                  shape:
                                                      new RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(20.0),
                                                  ),
                                                  onPressed: () {
                                                    provider.isAdsetLoading =
                                                        true;
                                                    adseTreatment();
                                                  },
                                                  color: Colors.blue,
                                                  child: Text(
                                                    'Valider',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                            ]))));
              }));
        }));
  }
}
