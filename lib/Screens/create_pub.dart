import 'dart:convert';

import 'package:abysterpub/Helpers/dropdown_formfield.dart';
import 'package:abysterpub/Helpers/formValidator.dart';
import 'package:abysterpub/models/publicite.dart';
import 'package:abysterpub/states/facebookStates.dart';
import 'package:abysterpub/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:abysterpub/Helpers/Dropdown.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:crypto/crypto.dart';
import 'package:provider/provider.dart';
import 'package:flutter_flexible_toast/flutter_flexible_toast.dart';

class Create_pub extends StatefulWidget {
  dynamic id;
  Create_pub(this.id);
  @override
  Create_pubState createState() => Create_pubState();
}

class Create_pubState extends State<Create_pub> {
  void iniState() {
    // _dropdownMenuActionItems = buildDropdownActionMenuItems(_actions);
    // _selectedActions = null;
    FilePickerCross.listInternalFiles()
        .then((value) => setState(() => lastFiles = value.toSet()));
    FilePickerCross.quota().then((value) => setState(() => quota = value));
    super.initState();
    _selected = 0;
  }

  GlobalKey<FormState> _key = new GlobalKey();

  TextEditingController nameController = new TextEditingController();
  TextEditingController messageController = new TextEditingController();
  TextEditingController ctoController = new TextEditingController();
  TextEditingController desController = new TextEditingController();
  TextEditingController titreController = new TextEditingController();

  Ads _ads;
  FacebookStates face;
  String _myTarget;
  int _selected;
  void onChanged(int value) {
    setState(() {
      _selected = value;
      _myTarget = '';
    });
  }

  void adsTreatment() async {
    if (_key.currentState.validate()) {
      _key.currentState.save();
      await _ads
          .createAds(
              nameController.text, ctoController.text, messageController.text)
          .then((creativeId) {
        if (creativeId == 'error') {
          face.isAdLoading = false;
          showToast(
              "Erreur, vos informations n'ont pas été transmise à Facebook !",
              ICON.ERROR);
        } else {
          _ads.createAds2(widget.id, creativeId).then((response) {
            if (response == 'error') {
              face.isAdLoading = false;
              showToast(
                  "Erreur, vos informations n'ont pas été transmise à Facebook !",
                  ICON.ERROR);
            } else {
              face.isAdLoading = false;
              showToast("Groupe de publicité crée avec succès !", ICON.SUCCESS);
              // _ads.saveAds();
            }
          });
        }
      });
    }
  }

  void showToast(String message, ICON icon) {
    FlutterFlexibleToast.showToast(
        message: message,
        toastLength: Toast.LENGTH_LONG,
        toastGravity: ToastGravity.BOTTOM,
        icon: icon,
        radius: 100,
        elevation: 10,
        imageSize: 35,
        textColor: Colors.white,
        backgroundColor: Colors.black,
        timeInSeconds: 2);
  }
  // List<Action2> _actions = Action2.getAction2();
  // List<DropdownMenuItem<Action2>> _dropdownMenuActionItems;
  // Action2 _selectedActions;

  // List<DropdownMenuItem<Action2>> buildDropdownActionMenuItems(List actions) {
  //   List<DropdownMenuItem<Action2>> items = List();
  //   for (Action2 action in actions) {
  //     items.add(
  //       DropdownMenuItem(
  //         value: action,
  //         child: Text(action.name),
  //       ),
  //     );
  //   }
  //   return items;
  // }

  // onChangeDropdownItem(Action2 selectedActions) {
  //   setState(() {
  //     _selectedActions = selectedActions;
  //   });
  // }

  void _selectFile() {
    FilePickerCross.importFromStorage()
        .then((filePicker) => setFilePicker(filePicker));
  }

  void _selectSaveFile() {
    filePickerCross.exportToStorage();
  }

  FilePickerCross filePickerCross;

  String _fileString = '';
  Set<String> lastFiles;
  FileQuotaCross quota = FileQuotaCross(quota: 0, usage: 0);

  setFilePicker(FilePickerCross filePicker) => setState(() {
        filePickerCross = filePicker;
        filePickerCross.saveToPath(path: filePickerCross.fileName);
        FilePickerCross.quota().then((value) {
          print(value);
          setState(() => quota = value);
        });
        lastFiles.add(filePickerCross.fileName);
        try {
          _fileString = filePickerCross.toString();
        } catch (e) {
          _fileString = 'Not a text file. Showing base64.\n\n' +
              filePickerCross.toBase64();
        }
      });

  @override
  Widget build(BuildContext context) {
    var iskeyboardloaded = MediaQuery.of(context).viewInsets.bottom;
    var hauteurEcran = MediaQuery.of(context).size.height;
    var largeurEcran = MediaQuery.of(context).size.width;
    return ChangeNotifierProvider<FacebookStates>(
        create: (context) => FacebookStates(),
        child: Builder(builder: (context) {
          return Scaffold(
              backgroundColor: Colors.white,
              drawer: SideBar(),
              appBar: GradientAppBar(
                title: Text("AbysterPub"),
                backgroundColorStart: Colors.blue,
                backgroundColorEnd: Colors.teal,
              ),
              body:
                  Consumer<FacebookStates>(builder: (context, provider, child) {
                return AbysterPubLoader(
                    inAsyncCall: provider.isAdLoading,
                    child: Center(
                        child: Container(
                            height: MediaQuery.of(context).size.height,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topRight,
                                    end: Alignment.bottomLeft,
                                    colors: [Colors.blue, Colors.teal])),
                            child: Column(children: <Widget>[
                              Expanded(
                                  child: Padding(
                                      padding: (iskeyboardloaded == 0.0)
                                          ? EdgeInsets.fromLTRB(
                                              largeurEcran / 70,
                                              hauteurEcran / 5,
                                              largeurEcran / 70,
                                              hauteurEcran / 3)
                                          : EdgeInsets.fromLTRB(
                                              largeurEcran / 70,
                                              hauteurEcran / 12,
                                              largeurEcran / 70,
                                              hauteurEcran / 9),
                                      child: Container(
                                          child: Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        elevation: 4.0,
                                        color: Colors.white,
                                        child: ListView(
                                          shrinkWrap: true,
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: TextFormField(
                                                            controller:
                                                                nameController,
                                                            showCursor: true,
                                                            decoration:
                                                                InputDecoration(
                                                                    hintText:
                                                                        'Nom pub'),
                                                          ))),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: TextFormField(
                                                            onSaved:
                                                                (String value) {
                                                              _ads.message =
                                                                  value;
                                                            },
                                                            controller:
                                                                messageController,
                                                            showCursor: true,
                                                            decoration:
                                                                InputDecoration(
                                                                    hintText:
                                                                        'Message'),
                                                          ))),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: TextFormField(
                                                            onSaved:
                                                                (String value) {
                                                              _ads.titre =
                                                                  value;
                                                            },
                                                            controller:
                                                                titreController,
                                                            showCursor: true,
                                                            decoration:
                                                                InputDecoration(
                                                                    hintText:
                                                                        'titre'),
                                                          ))),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: TextFormField(
                                                            onSaved:
                                                                (String value) {
                                                              _ads.description =
                                                                  value;
                                                            },
                                                            controller:
                                                                desController,
                                                            showCursor: true,
                                                            decoration:
                                                                InputDecoration(
                                                                    hintText:
                                                                        'description'),
                                                          ))),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      child: Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: DropDownFormField(
                                                      validator: FormValidator()
                                                          .validateOtherFields,
                                                      hintText:
                                                          'appel à action',
                                                      value: _myTarget,
                                                      onSaved: (value) {
                                                        setState(() {
                                                          _ads.cto = value;
                                                        });
                                                      },
                                                      onChanged: (value) {
                                                        setState(() {
                                                          _myTarget = value;
                                                        });
                                                      },
                                                      dataSource: [
                                                        {
                                                          "display":
                                                              "Commander maintenant",
                                                          "value": "ORDER_NOW",
                                                        },
                                                        {
                                                          "display":
                                                              "Installation d'apps",
                                                          "value":
                                                              "INSTALL_MOBILE_APP",
                                                        },
                                                        {
                                                          "display":
                                                              "Souscrire",
                                                          "value": "SUBSCRIBE",
                                                        },
                                                        {
                                                          "display":
                                                              "Ajouter au panier",
                                                          "value":
                                                              "ADD_TO_CART",
                                                        },
                                                      ],
                                                      textField: 'display',
                                                      valueField: 'value',
                                                    ),
                                                  )),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Container(
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: TextFormField(
                                                          readOnly: true,
                                                          showCursor: true,
                                                          decoration:
                                                              InputDecoration(
                                                                  suffixIcon:
                                                                      IconButton(
                                                                    icon: Icon(Icons
                                                                        .attach_file),
                                                                    color: Colors
                                                                        .blue,
                                                                    onPressed:
                                                                        _selectFile,
                                                                  ),
                                                                  hintText: (filePickerCross !=
                                                                          null)
                                                                      ? '${filePickerCross.fileName}'
                                                                      : 'nom du média'),
                                                        )),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal:
                                                                largeurEcran /
                                                                    11),
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      child: Container(
                                                        child: RaisedButton(
                                                          shape:
                                                              new RoundedRectangleBorder(
                                                            borderRadius:
                                                                new BorderRadius
                                                                        .circular(
                                                                    20.0),
                                                          ),
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                          color: Colors.blue,
                                                          child: Text(
                                                            'Annuler',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 18),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    child: Container(
                                                      child: RaisedButton(
                                                        shape:
                                                            new RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  20.0),
                                                        ),
                                                        onPressed: () {
                                                          provider.isAdLoading =
                                                              true;
                                                          adsTreatment();
                                                          // createAdCreative(
                                                          //     filePickerCross.path);

                                                          // createAds(nameController.text,
                                                          //     filePickerCross.toBase64());
                                                          // print(filePickerCross.toBase64());
                                                          // print(sha256
                                                          //     .convert(
                                                          //         filePickerCross.toUint8List())
                                                          //     .toString());
                                                          //createAds();
                                                        },
                                                        color: Colors.blue,
                                                        child: Text(
                                                          'Creer pub',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 18),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ))))
                            ]))));
              }));
        }));
  }
}

// Padding(
//     padding: const EdgeInsets.all(20.0),
//     child: Column(
//       children: <Widget>[
//         Row(
//           children: <Widget>[
//             Text("Placements"),
//             CheckboxListTile(
//                 title: Text("Facebook"),
//                 value: false,
//                 onChanged: (bool value) {
//                   value = false;
//                 }),
//             CheckboxListTile(
//                 title: Text("Instagram"),
//                 value: false,
//                 onChanged: (bool value) {
//                   value = false;
//                 }),
//           ],
//         ),
//         Row(
//           children: <Widget>[
//             CheckboxListTile(
//                 title: Text("Messenger"),
//                 value: false,
//                 onChanged: (bool value) {
//                   value = false;
//                 }),
//             CheckboxListTile(
//                 title: Text("Audience Network"),
//                 value: false,
//                 onChanged: (bool value) {
//                   value = false;
//                 }),
//           ],
//         ),
//       ],
//     )),
// Padding(
//   padding: const EdgeInsets.all(20.0),
//   child: Row(
//     children: <Widget>[
//       IconButton(
//           icon: Icon(Icons.lock), onPressed: null),
//       Expanded(
//           child: Container(
//               margin:
//                   EdgeInsets.only(right: 20, left: 10),
//               child: TextField(
//                 decoration:
//                     InputDecoration(hintText: 'Titre'),
//               ))),
//     ],
//   ),
// ),
