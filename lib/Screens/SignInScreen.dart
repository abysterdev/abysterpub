import 'package:abysterpub/Helpers/formValidator.dart';
import 'package:abysterpub/models/utilisateur.dart';
import 'package:abysterpub/restClient.dart';
import 'package:abysterpub/states/authProvider.dart';
import 'package:abysterpub/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:provider/provider.dart';

import 'Welcome.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  AuthProvider _auth = new AuthProvider();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordcontroller = new TextEditingController();
  TextEditingController emailResetcontroller = new TextEditingController();
  GlobalKey<FormState> _key = new GlobalKey();
  GlobalKey<FormState> _key2 = new GlobalKey();
  Utilisateur _user = new Utilisateur();
  bool passwordVisible;
  bool loginvisible = false;
  void initState() {
    passwordVisible = true;
    loginvisible = false;
    super.initState();
  }

  void changePasswordVisibility(bool val) {
    setState(() {
      passwordVisible = val;
    });
  }

  Future<void> _ackAlert(String title, String message) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            title,
            style: TextStyle(color: Colors.blue),
          ),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Fermer', style: TextStyle(color: Colors.teal)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void login() {
    if (_key.currentState.validate()) {
      _key.currentState.save();
      _user.login().then((response) async {
        if (response == 'error') {
          _auth.isLoginPageLoading = false;
          _ackAlert(
              "erreur", "Une erreur s'est produite lors de l'authentification");
        } else {
          await RestClient.persistStringDataInStorage('isLoggedIn', 'true');
          _auth.isLoginPageLoading = false;
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (BuildContext context) => Welcome()));
        }
      });
    }
  }

  void sendOtp() {
    if (_key2.currentState.validate()) {
      _key2.currentState.save();
      _user.requestOtp().then((response) async {
        if (response == 'error') {
          _auth.isOTPPageLoading = false;
          _ackAlert(
              "erreur", "Une erreur s'est produite lors de l'authentification");
        } else {
          await RestClient.persistStringDataInStorage('isLoggedIn', 'true');
          _auth.isOTPPageLoading = false;
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (BuildContext context) => Welcome()));
        }
      });
    }
  }

  // void validateResetForm(String email) {
  //   if (EmailValidator.validate(email)) {
  //     authprovider.requestOtp(email);
  //   }
  // }

  createAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return ChangeNotifierProvider<AuthProvider>(
              create: (context) => AuthProvider(),
              child: Builder(builder: (context) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  contentPadding: EdgeInsets.all(15.0),
                  title: Text("Password Reset",
                      style: TextStyle(color: Colors.teal)),
                  content: StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) {
                    return Form(
                        key: _key2,
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                            onSaved: (String value) {
                              _user.email = value;
                            },
                            validator: FormValidator().validateEmail,
                            controller: emailResetcontroller,
                            showCursor: true,
                            decoration: InputDecoration(hintText: 'Email'),
                          ),
                        ));
                  }),
                  actions: <Widget>[
                    Consumer<AuthProvider>(builder: (context, provider, child) {
                      return (provider.isOTPPageLoading)
                          ? Container(
                              child: Center(
                              child: AbysterPubLoader(
                                  inAsyncCall: provider.isOTPPageLoading,
                                  child: null),
                            ))
                          : Text("");
                    }),
                    new FlatButton(
                        child: new Text("Envoyer le code"),
                        onPressed: () {
                          _auth.isOTPPageLoading = true;
                          sendOtp();
                          // validateResetForm(emailResetcontroller.text);
                          // (authprovider.isOTPsent)
                          //     ? Navigator.pushNamed(context, 'passwordReset')
                          //     : print("");
                        }),
                  ],
                );
              }));
        });
  }

  @override
  Widget build(BuildContext context) {
    //var state = Provider.of<AuthProvider>(context, listen: false);
    return ChangeNotifierProvider<AuthProvider>(
        create: (context) => AuthProvider(),
        child: Builder(builder: (context) {
          return Scaffold(
              backgroundColor: Colors.white,
              body: Consumer<AuthProvider>(builder: (context, provider, child) {
                return AbysterPubLoader(
                  inAsyncCall: provider.isLoginPageLoading,
                  child: SingleChildScrollView(
                    child: Center(
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [Colors.blue, Colors.teal])),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 100.0, bottom: 0.0),
                              child: Image.asset(
                                'asset/img/téléchargement.jpg',
                                height: 100,
                                width: 100,
                              ),
                            ),
                            Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                margin: EdgeInsets.fromLTRB(35, 3, 35, 30),
                                elevation: 4.0,
                                color: Colors.white,
                                child: Form(
                                  key: _key,
                                  child: ListView(
                                    shrinkWrap: true,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(20.0),
                                        child: Row(
                                          children: <Widget>[
                                            IconButton(
                                                icon: Icon(Icons.person),
                                                onPressed: null),
                                            Expanded(
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 0, left: 5),
                                                    child: TextFormField(
                                                      validator: FormValidator()
                                                          .validateEmail,
                                                      controller:
                                                          emailController,
                                                      decoration:
                                                          InputDecoration(
                                                              hintText:
                                                                  'email'),
                                                    )))
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(20.0),
                                        child: Row(children: <Widget>[
                                          IconButton(
                                              icon: Icon(Icons.lock),
                                              onPressed: null),
                                          Expanded(
                                              child: Container(
                                                  margin: EdgeInsets.only(
                                                      right: 0, left: 5),
                                                  child: (passwordVisible)
                                                      ? TextFormField(
                                                          validator: FormValidator()
                                                              .validatePassword,
                                                          obscureText: true,
                                                          showCursor: true,
                                                          controller:
                                                              passwordcontroller,
                                                          decoration: InputDecoration(
                                                              suffixIcon: IconButton(
                                                                  color: Colors
                                                                      .black12,
                                                                  icon: Icon(Icons
                                                                      .visibility_off),
                                                                  onPressed: () =>
                                                                      changePasswordVisibility(
                                                                          false)),
                                                              hintText:
                                                                  'Mot de passe'),
                                                        )
                                                      : TextFormField(
                                                          validator: FormValidator()
                                                              .validatePassword,
                                                          obscureText: false,
                                                          showCursor: true,
                                                          controller:
                                                              passwordcontroller,
                                                          decoration: InputDecoration(
                                                              suffixIcon: IconButton(
                                                                  color: Colors
                                                                      .blue,
                                                                  icon: Icon(Icons
                                                                      .remove_red_eye),
                                                                  onPressed: () =>
                                                                      changePasswordVisibility(
                                                                          true)),
                                                              hintText:
                                                                  'Mot de passe'),
                                                        ))),
                                        ]),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(20.0),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: Container(
                                            margin: EdgeInsets.fromLTRB(
                                                0.0, 0.0, 0.0, 6.0),
                                            child: RaisedButton(
                                              padding: EdgeInsets.only(
                                                  right: 1, left: 1),
                                              shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        20.0),
                                              ),
                                              onPressed: () {
                                                //_auth.isLoginPageLoading = true;
                                                // provider.isLoginPageLoading =
                                                // true;
                                                // _auth.isLoginPageLoading = true;
                                                // print(provider
                                                //     .isLoginPageLoadingState);
                                                //login();
                                                Navigator.of(context)
                                                    .pushReplacement(
                                                        MaterialPageRoute(
                                                            builder: (BuildContext
                                                                    context) =>
                                                                Welcome()));
                                              },
                                              color: Colors.blue,
                                              child: Text(
                                                'Se Connecter',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 18),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, 'SignUp');
                              },
                              child: Center(
                                child: RichText(
                                  text: TextSpan(
                                      text: 'Pas de compte? ',
                                      style: TextStyle(color: Colors.white),
                                      children: [
                                        TextSpan(
                                          text: 'Inscrivez-vous',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ]),
                                ),
                              ),
                            ),
                            Text(""),
                            InkWell(
                              onTap: () {
                                createAlertDialog(context);
                              },
                              child: Center(
                                child: RichText(
                                  text: TextSpan(
                                      text: 'Mot de passe oublié ? ',
                                      style: TextStyle(color: Colors.white),
                                      children: [
                                        TextSpan(
                                          text: 'Réinitialiser',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ]),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }));
        }));
  }
}
