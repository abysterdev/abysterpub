import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Packages extends StatelessWidget {
  final _currencies = ['Rupees', 'Dollars', 'Pounds', 'Others'];
  final _currentItemSelected = 'Dollars';
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          drawer: SideBar(),
          appBar: GradientAppBar(
            title: Text("AbysterPub"),
            backgroundColorStart: Colors.blue,
            backgroundColorEnd: Colors.teal,
            bottom: TabBar(
              labelStyle: TextStyle(fontSize: 18),
              tabs: [
                Tab(text: "Carousel"),
                Tab(text: "Videos/images"),
                Tab(text: "Collections"),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Scaffold(
                body: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: height / 1.5,
                          width: width / 1.15,
                          child: Carousel(
                            boxFit: BoxFit.cover,
                            autoplay: true,
                            animationCurve: Curves.fastOutSlowIn,
                            animationDuration: Duration(milliseconds: 500),
                            dotSize: 6.0,
                            dotIncreasedColor: Color(0xFFFF335C),
                            dotBgColor: Colors.transparent,
                            dotPosition: DotPosition.topRight,
                            dotVerticalPadding: 10.0,
                            showIndicator: false,
                            indicatorBgPadding: 7.0,
                            images: [
                              Material(
                                  child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, "custom_package");
                                },
                                child: Container(
                                  height: 500,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset('asset/img/video1.png',
                                        width: 400.0, height: 500.0),
                                  ),
                                ),
                              )),
                              Material(
                                  child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, "custom_package");
                                },
                                child: Container(
                                  height: 500,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset('asset/img/video2.png',
                                        width: 400.0, height: 500.0),
                                  ),
                                ),
                              )),
                              Material(
                                  child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, "custom_package");
                                },
                                child: Container(
                                  height: 500,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset('asset/img/video1.png',
                                        width: 400.0, height: 500.0),
                                  ),
                                ),
                              )),
                            ],
                          ),
                        ),
                      ]),
                ),
              ),
              Scaffold(
                body: Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 500.0,
                          width: 300.0,
                          child: Carousel(
                            boxFit: BoxFit.cover,
                            autoplay: true,
                            animationCurve: Curves.fastOutSlowIn,
                            animationDuration: Duration(milliseconds: 500),
                            dotSize: 6.0,
                            dotIncreasedColor: Color(0xFFFF335C),
                            dotBgColor: Colors.transparent,
                            dotPosition: DotPosition.topRight,
                            dotVerticalPadding: 10.0,
                            showIndicator: false,
                            indicatorBgPadding: 7.0,
                            images: [
                              Material(
                                  child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, "custom_package");
                                },
                                child: Container(
                                  height: 500,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset('asset/img/video1.png',
                                        width: 400.0, height: 500.0),
                                  ),
                                ),
                              )),
                              Material(
                                  child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, "custom_package");
                                },
                                child: Container(
                                  height: 500,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset('asset/img/video2.png',
                                        width: 400.0, height: 500.0),
                                  ),
                                ),
                              )),
                              Material(
                                  child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, "custom_package");
                                },
                                child: Container(
                                  height: 500,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset('asset/img/video1.png',
                                        width: 400.0, height: 500.0),
                                  ),
                                ),
                              )),
                            ],
                          ),
                        ),
                      ]),
                ),
              ),
              Scaffold(
                body: Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 500.0,
                          width: 300.0,
                          child: Carousel(
                            boxFit: BoxFit.cover,
                            autoplay: true,
                            animationCurve: Curves.fastOutSlowIn,
                            animationDuration: Duration(milliseconds: 500),
                            dotSize: 6.0,
                            dotIncreasedColor: Color(0xFFFF335C),
                            dotBgColor: Colors.transparent,
                            dotPosition: DotPosition.topRight,
                            dotVerticalPadding: 10.0,
                            showIndicator: false,
                            indicatorBgPadding: 7.0,
                            images: [
                              Material(
                                  child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, "custom_package");
                                },
                                child: Container(
                                  height: 500,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset('asset/img/video1.png',
                                        width: 400.0, height: 500.0),
                                  ),
                                ),
                              )),
                              Material(
                                  child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, "custom_package");
                                },
                                child: Container(
                                  height: 500,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset('asset/img/video2.png',
                                        width: 400.0, height: 500.0),
                                  ),
                                ),
                              )),
                              Material(
                                  child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, "custom_package");
                                },
                                child: Container(
                                  height: 500,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset('asset/img/video1.png',
                                        width: 400.0, height: 500.0),
                                  ),
                                ),
                              )),
                            ],
                          ),
                        ),
                      ]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
