import 'package:abysterpub/Helpers/dropdown_formfield.dart';
import 'package:abysterpub/Helpers/formValidator.dart';
import 'package:abysterpub/states/authProvider.dart';
import 'package:abysterpub/widgets/loader.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:abysterpub/Helpers/Dropdown.dart';
import 'package:provider/provider.dart';
import 'package:abysterpub/models/utilisateur.dart';

import 'Welcome.dart';
//import 'package:font_awesome_flutter_example/icons.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String _role;
  Utilisateur _user = Utilisateur();
  final logincontroller = TextEditingController();
  final passwordcontroller = TextEditingController();
  final confirmpasswordcontroller = TextEditingController();
  GlobalKey<FormState> _key = new GlobalKey();
  AuthProvider _auth = AuthProvider();
  bool passwordVisible;
  bool passwordVisible2;

  void initState() {
    super.initState();
    _role = '';
    passwordVisible = true;
  }

  void changePasswordVisibility(bool val) {
    setState(() {
      passwordVisible = val;
    });
  }

  void changePasswordVisibility2(bool val) {
    setState(() {
      passwordVisible2 = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    var state = Provider.of<AuthProvider>(context);
    var hauteurEcran = MediaQuery.of(context).size.height;
    var largeurEcran = MediaQuery.of(context).size.width;

    Future<void> _ackAlert(String title, String message) {
      return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text('Fermer'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    void signIn() {
      if (_key.currentState.validate()) {
        _key.currentState.save();
        _user.createUser().then((response) {
          if (response == 'error') {
            _auth.isRegisterPageLoading = false;
            _ackAlert(
                "Erreur !", "une erreur est survenue lors de l'inscription");
          } else {
            _auth.isRegisterPageLoading = false;
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (BuildContext context) => Welcome()),
                (Route<dynamic> route) => false);
          }
        });
      }
    }

    return ChangeNotifierProvider<AuthProvider>(
        create: (context) => AuthProvider(),
        child: Builder(builder: (context) {
          return Scaffold(
              backgroundColor: Colors.white,
              body: Consumer<AuthProvider>(builder: (context, provider, child) {
                return AbysterPubLoader(
                  inAsyncCall: provider.isRegisterPageLoading,
                  child: SingleChildScrollView(
                    child: Center(
                      child: Container(
                        height: hauteurEcran,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [Colors.blue, Colors.teal])),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 100.0, bottom: 0.0),
                              child: Image.asset(
                                'asset/img/téléchargement.jpg',
                                height: 100,
                                width: 100,
                              ),
                            ),
                            Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              margin: EdgeInsets.fromLTRB(
                                  largeurEcran / 9, 3, largeurEcran / 9, 30),
                              elevation: 4.0,
                              color: Colors.white,
                              child: Form(
                                key: _key,
                                child: ListView(
                                  shrinkWrap: true,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Row(
                                        children: <Widget>[
                                          IconButton(
                                              icon: Icon(Icons.person),
                                              onPressed: null),
                                          Expanded(
                                              child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: TextFormField(
                                                    validator: FormValidator()
                                                        .validateEmail,
                                                    showCursor: true,
                                                    controller: logincontroller,
                                                    decoration: InputDecoration(
                                                        hintText: 'email'),
                                                  ))),
                                        ],
                                      ),
                                    ),
                                    Row(children: <Widget>[
                                      IconButton(
                                          icon: Icon(Icons.person),
                                          onPressed: null),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: DropDownFormField(
                                            hintText: 'role',
                                            value: _role,
                                            onSaved: (value) {
                                              setState(() {
                                                _role = value;
                                              });
                                            },
                                            onChanged: (value) {
                                              setState(() {
                                                _role = value;
                                              });
                                            },
                                            dataSource: [
                                              {
                                                "display": "Admin",
                                                "value": "Admin",
                                              },
                                              {
                                                "display": "Agent",
                                                "value": "Agent",
                                              },
                                            ],
                                            textField: 'display',
                                            valueField: 'value',
                                          ),
                                        ),
                                      ),
                                    ]),
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Row(
                                        children: <Widget>[
                                          IconButton(
                                              icon: Icon(Icons.lock),
                                              onPressed: null),
                                          Expanded(
                                              child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: (passwordVisible ==
                                                          true)
                                                      ? TextFormField(
                                                          validator: FormValidator()
                                                              .validatePassword,
                                                          obscureText: true,
                                                          showCursor: true,
                                                          controller:
                                                              passwordcontroller,
                                                          decoration: InputDecoration(
                                                              suffixIcon: IconButton(
                                                                  color: Colors
                                                                      .black12,
                                                                  icon: Icon(Icons
                                                                      .visibility_off),
                                                                  onPressed: () =>
                                                                      changePasswordVisibility(
                                                                          false)),
                                                              hintText:
                                                                  'Mot de passe'),
                                                        )
                                                      : TextFormField(
                                                          validator: FormValidator()
                                                              .validatePassword,
                                                          obscureText: false,
                                                          showCursor: true,
                                                          controller:
                                                              passwordcontroller,
                                                          decoration: InputDecoration(
                                                              suffixIcon: IconButton(
                                                                  color: Colors
                                                                      .blue,
                                                                  icon: Icon(Icons
                                                                      .remove_red_eye),
                                                                  onPressed: () =>
                                                                      changePasswordVisibility(
                                                                          true)),
                                                              hintText:
                                                                  'Mot de passe'),
                                                        ))),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Row(
                                        children: <Widget>[
                                          IconButton(
                                              icon: Icon(Icons.lock),
                                              onPressed: null),
                                          Expanded(
                                              child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: (passwordVisible2 ==
                                                          true)
                                                      ? TextFormField(
                                                          validator: FormValidator()
                                                              .validatePassword,
                                                          obscureText: true,
                                                          showCursor: true,
                                                          controller:
                                                              confirmpasswordcontroller,
                                                          decoration: InputDecoration(
                                                              suffixIcon: IconButton(
                                                                  color: Colors
                                                                      .black12,
                                                                  icon: Icon(Icons
                                                                      .visibility_off),
                                                                  onPressed: () =>
                                                                      changePasswordVisibility2(
                                                                          false)),
                                                              hintText:
                                                                  'Confirmation Mot de passe'),
                                                        )
                                                      : TextFormField(
                                                          validator: FormValidator()
                                                              .validatePassword,
                                                          obscureText: false,
                                                          showCursor: true,
                                                          controller:
                                                              confirmpasswordcontroller,
                                                          decoration: InputDecoration(
                                                              suffixIcon: IconButton(
                                                                  color: Colors
                                                                      .blue,
                                                                  icon: Icon(Icons
                                                                      .remove_red_eye),
                                                                  onPressed: () =>
                                                                      changePasswordVisibility2(
                                                                          true)),
                                                              hintText:
                                                                  'Confirmation Mot de passe'),
                                                        ))),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(5),
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              0.0, 0.0, 0.0, 6.0),
                                          child: RaisedButton(
                                            padding: EdgeInsets.only(
                                                right: 1, left: 1),
                                            shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      20.0),
                                            ),
                                            onPressed: () {
                                              provider.isRegisterPageLoading =
                                                  true;
                                              signIn();
                                              // : print("");
                                            },
                                            color: Colors.blue,
                                            child: Text(
                                              'S\'inscrire',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, 'SignIn');
                              },
                              child: Center(
                                child: RichText(
                                  text: TextSpan(
                                      text: 'Vous avez déjà un compte ? ',
                                      style: TextStyle(color: Colors.white),
                                      children: [
                                        TextSpan(
                                          text: 'Connectez-vous',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ]),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }));
        }));
  }
}

// class BackButtonWidget extends StatelessWidget {
//   const BackButtonWidget({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 200,
//       decoration: BoxDecoration(
//           image: DecorationImage(
//               fit: BoxFit.cover, image: AssetImage('asset/img/app.png'))),
//       child: Positioned(
//           child: Stack(
//         children: <Widget>[
//           Positioned(
//               top: 20,
//               child: Row(
//                 children: <Widget>[
//                   IconButton(
//                       icon: Icon(
//                         Icons.arrow_back_ios,
//                         color: Colors.white,
//                       ),
//                       onPressed: () {
//                         Navigator.pop(context);
//                       }),
//                   Text(
//                     'Back',
//                     style: TextStyle(
//                         color: Colors.white, fontWeight: FontWeight.bold),
//                   )
//                 ],
//               )),
//           Positioned(
//             bottom: 20,
//             child: Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: Text(
//                 'Create New Account',
//                 style: TextStyle(
//                     color: Colors.white,
//                     fontWeight: FontWeight.bold,
//                     fontSize: 18),
//               ),
//             ),
//           )
//         ],
//       )),
//     );
//   }
// }
