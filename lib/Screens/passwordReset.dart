import 'package:abysterpub/Helpers/formValidator.dart';
import 'package:abysterpub/Screens/SignInScreen.dart';
import 'package:abysterpub/models/utilisateur.dart';
import 'package:abysterpub/states/authProvider.dart';
import 'package:abysterpub/widgets/loader.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PasswordReset extends StatefulWidget {
  @override
  _PasswordResetState createState() => new _PasswordResetState();
}

class _PasswordResetState extends State<PasswordReset> {
  AuthProvider authprovider = new AuthProvider();
  TextEditingController otpController = new TextEditingController();
  TextEditingController emailcontroller = new TextEditingController();
  TextEditingController passwordconfirmcontroller = new TextEditingController();
  TextEditingController passwordcontroller = new TextEditingController();
  GlobalKey<FormState> _key = new GlobalKey();
  Utilisateur _user = Utilisateur();
  bool passwordVisible;
  void initState() {
    super.initState();
    passwordVisible = true;
  }

  void changePasswordVisibility(bool val) {
    setState(() {
      passwordVisible = val;
    });
  }

  Future<void> _ackAlert(String title, String message) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Fermer'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void resetPassword() {
    if (_key.currentState.validate()) {
      _key.currentState.save();
      _user.changePassword().then((response) {
        if (response == 'error') {
          authprovider.isChangePasswordLoading = false;
          _ackAlert("erreur", "une erreur est survenue lors de l'opération");
        } else {
          authprovider.isChangePasswordLoading = false;
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (BuildContext context) => SignInScreen()));
        }
      });
    }
  }

  // void validateResetForm(String email, String password, String otp) {
  //   if (EmailValidator.validate(email)) {
  //     authprovider.changePassword(email, password, otp);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AuthProvider>(
        create: (context) => AuthProvider(),
        child: Builder(builder: (context) {
          return new Scaffold(
              body: Consumer<AuthProvider>(builder: (context, provider, child) {
            return AbysterPubLoader(
                inAsyncCall: provider.isChangePasswordLoading,
                child: SingleChildScrollView(
                    child: Center(
                        child: Container(
                            height: MediaQuery.of(context).size.height,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topRight,
                                    end: Alignment.bottomLeft,
                                    colors: [Colors.blue, Colors.teal])),
                            child: Column(children: <Widget>[
                              IconButton(
                                padding: EdgeInsets.fromLTRB(0, 150, 0, 15),
                                icon: Icon(
                                  Icons.person,
                                  color: Colors.white,
                                ),
                                onPressed: null,
                                iconSize: 150,
                              ),
                              Expanded(
                                  child: Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      margin:
                                          EdgeInsets.fromLTRB(10, 0, 10, 200),
                                      elevation: 4.0,
                                      color: Colors.white,
                                      child: Form(
                                          key: _key,
                                          child: ListView(
                                              shrinkWrap: true,
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets.all(
                                                      10.0),
                                                  child: Row(
                                                    children: <Widget>[
                                                      IconButton(
                                                          icon:
                                                              Icon(Icons.email),
                                                          onPressed: null),
                                                      Expanded(
                                                          child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right: 20,
                                                                      left: 10),
                                                              child:
                                                                  TextFormField(
                                                                onSaved: (String
                                                                    value) {
                                                                  _user.otp =
                                                                      value;
                                                                },
                                                                decoration:
                                                                    InputDecoration(
                                                                  hintText:
                                                                      "code",
                                                                ),
                                                              )))
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.all(
                                                      10.0),
                                                  child: Row(
                                                    children: <Widget>[
                                                      IconButton(
                                                          icon:
                                                              Icon(Icons.email),
                                                          onPressed: null),
                                                      Expanded(
                                                          child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right: 20,
                                                                      left: 10),
                                                              child:
                                                                  TextFormField(
                                                                onSaved: (String
                                                                    value) {
                                                                  _user.email =
                                                                      value;
                                                                },
                                                                decoration:
                                                                    InputDecoration(
                                                                  hintText:
                                                                      "email",
                                                                ),
                                                              )))
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.all(
                                                      20.0),
                                                  child: Row(
                                                    children: <Widget>[
                                                      IconButton(
                                                          icon:
                                                              Icon(Icons.lock),
                                                          onPressed: null),
                                                      Expanded(
                                                          child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right: 20,
                                                                      left: 10),
                                                              child: (passwordVisible)
                                                                  ? TextFormField(
                                                                      obscureText:
                                                                          true,
                                                                      showCursor:
                                                                          true,
                                                                      controller:
                                                                          emailcontroller,
                                                                      decoration:
                                                                          InputDecoration(
                                                                              hintText: 'Nouveau Mot de passe'),
                                                                    )
                                                                  : TextFormField(
                                                                      obscureText:
                                                                          false,
                                                                      showCursor:
                                                                          true,
                                                                      controller:
                                                                          emailcontroller,
                                                                      decoration:
                                                                          InputDecoration(
                                                                              hintText: 'Nouveau Mot de passe'),
                                                                    ))),
                                                      (passwordVisible)
                                                          ? IconButton(
                                                              icon: Icon(Icons
                                                                  .visibility_off),
                                                              onPressed: () =>
                                                                  changePasswordVisibility(
                                                                      false))
                                                          : IconButton(
                                                              color:
                                                                  Colors.blue,
                                                              icon: Icon(Icons
                                                                  .remove_red_eye),
                                                              onPressed: () =>
                                                                  changePasswordVisibility(
                                                                      true))
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.all(
                                                      20.0),
                                                  child: Row(
                                                    children: <Widget>[
                                                      IconButton(
                                                          icon:
                                                              Icon(Icons.lock),
                                                          onPressed: null),
                                                      Expanded(
                                                          child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right: 20,
                                                                      left: 10),
                                                              child: (passwordVisible)
                                                                  ? TextFormField(
                                                                      validator:
                                                                          (value) {
                                                                        if (passwordcontroller.text !=
                                                                            value) {
                                                                          return "Les mots de passe doivent correspondre";
                                                                        }
                                                                      },
                                                                      onSaved:
                                                                          (String
                                                                              value) {
                                                                        _user.password =
                                                                            value;
                                                                      },
                                                                      obscureText:
                                                                          true,
                                                                      showCursor:
                                                                          true,
                                                                      controller:
                                                                          passwordconfirmcontroller,
                                                                      decoration:
                                                                          InputDecoration(
                                                                              hintText: 'Confirmation Mot de passe'),
                                                                    )
                                                                  : TextFormField(
                                                                      onSaved:
                                                                          (String
                                                                              value) {
                                                                        _user.password =
                                                                            value;
                                                                      },
                                                                      obscureText:
                                                                          false,
                                                                      showCursor:
                                                                          true,
                                                                      controller:
                                                                          passwordconfirmcontroller,
                                                                      decoration:
                                                                          InputDecoration(
                                                                              hintText: 'Confirmation Mot de passe'),
                                                                    ))),
                                                      (passwordVisible)
                                                          ? IconButton(
                                                              icon: Icon(Icons
                                                                  .visibility_off),
                                                              onPressed: () =>
                                                                  changePasswordVisibility(
                                                                      false))
                                                          : IconButton(
                                                              color:
                                                                  Colors.blue,
                                                              icon: Icon(Icons
                                                                  .remove_red_eye),
                                                              onPressed: () =>
                                                                  changePasswordVisibility(
                                                                      true)),
                                                    ],
                                                  ),
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: const EdgeInsets
                                                              .fromLTRB(
                                                          35, 10, 0.0, 10),
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        child: Container(
                                                          child: RaisedButton(
                                                            shape:
                                                                new RoundedRectangleBorder(
                                                              borderRadius:
                                                                  new BorderRadius
                                                                          .circular(
                                                                      20.0),
                                                            ),
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                            color: Colors.blue,
                                                            child: Text(
                                                              'Annuler',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 18),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets
                                                              .fromLTRB(
                                                          75, 10, 0.0, 10),
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        child: Container(
                                                          child: RaisedButton(
                                                            shape:
                                                                new RoundedRectangleBorder(
                                                              borderRadius:
                                                                  new BorderRadius
                                                                          .circular(
                                                                      20.0),
                                                            ),
                                                            onPressed: () {
                                                              provider.isChangePasswordLoading =
                                                                  true;
                                                              resetPassword();
                                                            },
                                                            color: Colors.blue,
                                                            child: Text(
                                                              'Réinitialiser',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 18),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ]))))
                            ])))));
          }));
        }));
  }
}
