import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Mespubs extends StatefulWidget {
  @override
  _MespubsState createState() => _MespubsState();
}

class _MespubsState extends State<Mespubs> {
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideBar(),
      appBar: GradientAppBar(
        title: Text("AbysterPub"),
        backgroundColorStart: Colors.blue,
        backgroundColorEnd: Colors.teal,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
        child: GridView.count(
          crossAxisCount: 2,
          padding: EdgeInsets.all(3.0),
          children: <Widget>[
            makeDashboardItem("Campagnes créées", 3),
            makeDashboardItem("Publicités en cours de paiement", 0),
            makeDashboardItem("Publicités en cours de diffusion", 1),
            makeDashboardItem("Publicités en cours de validation", 2),
          ],
        ),
      ),
    );
  }

  Card makeDashboardItem(String title, int i) {
    return Card(
      elevation: 1.0,
      margin: new EdgeInsets.all(6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(220, 220, 220, 1.0)),
        child: new InkWell(
          onTap: () {
            switch (i) {
              case 0:
                Navigator.pushNamed(context, 'package');
                break;
              case 1:
                Navigator.pushNamed(context, 'CreateCampaign');
                break;
              case 2:
                Navigator.pushNamed(context, 'Mespubs');
                break;
              case 3:
                Navigator.pushNamed(context, 'Mespubs');
                break;
              default:
                Navigator.pushNamed(context, 'Mespubs');
            }
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              SizedBox(height: 50.0),
              new Center(
                child: new Text(title,
                    style: new TextStyle(fontSize: 18.0, color: Colors.black)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
