import 'package:flutter/material.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Notifications extends StatefulWidget {
  @override
  _Notificationstate createState() => _Notificationstate();
}

class _Notificationstate extends State<Notifications> {
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideBar(),
      appBar: GradientAppBar(
        title: Text("AbysterPub"),
        backgroundColorStart: Colors.blue,
        backgroundColorEnd: Colors.teal,
      ),
      body: ListView.builder(
          itemCount: 10,
          itemBuilder: (context, index) {
            final ThemeData theme = Theme.of(context);
            return InkWell(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Row(children: <Widget>[
                      Padding(
                          padding: EdgeInsets.fromLTRB(18.0, 0.0, 0.0, 0.0)),
                      Text(
                        "Date",
                        style: theme.textTheme.title,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                      ),
                    ]),
                    Row(children: <Widget>[
                      Padding(
                          padding: EdgeInsets.fromLTRB(18.0, 0.0, 0.0, 0.0)),
                      Text(''),
                    ]),
                    Row(children: <Widget>[
                      Padding(
                          padding: EdgeInsets.fromLTRB(18.0, 0.0, 0.0, 0.0)),
                      Expanded(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                            Text(
                              "Message",
                              style: TextStyle(fontStyle: FontStyle.italic),
                              maxLines: 3,
                            ),
                          ])),
                    ]),
                    Divider(
                      thickness: 0.3,
                      color: Colors.black,
                    ),
                  ],
                ),
              ),
            );
          }),
      // Changed code
    );
  }
}
