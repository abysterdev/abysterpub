import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideBar(),
      appBar: GradientAppBar(
        title: Text("AbysterPub"),
        backgroundColorStart: Colors.blue,
        backgroundColorEnd: Colors.teal,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 60.0, horizontal: 2.0),
        child: GridView.count(
          crossAxisCount: 2,
          padding: EdgeInsets.all(3.0),
          children: <Widget>[
            makeDashboardItem("Packs de publicités", Icons.edit, 0),
            makeDashboardItem("Creer une publicite", Icons.add_circle, 1),
            makeDashboardItem("Spots publicitaires", Icons.ondemand_video, 2),
            makeDashboardItem("Mes publicités", Icons.cast_connected, 3),
            makeDashboardItem("Paiement", Icons.attach_money, 4),
            makeDashboardItem("Mon Profil", Icons.person_outline, 5),
          ],
        ),
      ),
    );
  }

  Card makeDashboardItem(String title, IconData icon, int i) {
    return Card(
      elevation: 1.0,
      margin: new EdgeInsets.all(6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(220, 220, 220, 1.0)),
        child: new InkWell(
          onTap: () {
            switch (i) {
              case 0:
                Navigator.pushNamed(context, 'package');
                break;
              case 1:
                Navigator.pushNamed(context, 'CreateCampaign');
                break;
              case 2:
                Navigator.pushNamed(context, 'spot');
                break;
              case 3:
                Navigator.pushNamed(context, 'pubDetails');
                break;
              case 4:
                Navigator.pushNamed(context, 'paiement');
                break;
              case 5:
                Navigator.pushNamed(context, 'profile');
                break;
              default:
                Navigator.pushNamed(context, '');
            }
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              SizedBox(height: 50.0),
              Center(
                  child: Icon(
                icon,
                size: 40.0,
                color: Colors.black,
              )),
              SizedBox(height: 20.0),
              new Center(
                child: new Text(title,
                    style: new TextStyle(fontSize: 18.0, color: Colors.black)),
              )
            ],
          ),
        ),
      ),
    );
  }
}

//  onImageTap:(){
//
//  },
