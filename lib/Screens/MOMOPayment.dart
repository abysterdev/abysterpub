import 'package:flutter/material.dart';

class MOMOPayment extends StatefulWidget {
  @override
  _MOMOPaymentState createState() => _MOMOPaymentState();
}

class _MOMOPaymentState extends State<MOMOPayment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [Colors.blue, Colors.teal])),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 100.0, bottom: 0.0),
                  child: Image.asset(
                    'asset/img/momo.jpg',
                    height: 100,
                    width: 100,
                  ),
                ),
                Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  margin: EdgeInsets.fromLTRB(35, 55, 35, 30),
                  elevation: 4.0,
                  color: Colors.white,
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Row(
                          children: <Widget>[
                            IconButton(
                                icon: Icon(Icons.person), onPressed: null),
                            Expanded(
                                child: Container(
                                    margin:
                                        EdgeInsets.only(right: 20, left: 10),
                                    child: TextField(
                                      decoration: InputDecoration(
                                          hintText: 'Numero de telephone'),
                                    )))
                          ],
                        ),
                      ),
                      Row(children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Container(
                              margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 6.0),
                              child: RaisedButton(
                                padding: EdgeInsets.only(right: 1, left: 1),
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(20.0),
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, 'payer');
                                },
                                color: Colors.blue,
                                child: Text(
                                  'retour',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Container(
                              margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 6.0),
                              child: RaisedButton(
                                padding: EdgeInsets.only(right: 1, left: 1),
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(20.0),
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, 'valider_code');
                                },
                                color: Colors.blue,
                                child: Text(
                                  'Valider',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ])
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
