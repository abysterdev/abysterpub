import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:abysterpub/Helpers/Dropdown.dart';

class Publish extends StatefulWidget {
  @override
  _PublishState createState() => _PublishState();
}

class _PublishState extends State<Publish> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideBar(),
      appBar: GradientAppBar(
        title: Text("AbysterPub"),
        backgroundColorStart: Colors.blue,
        backgroundColorEnd: Colors.teal,
      ),
      body: Center(
          child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Colors.blue, Colors.teal])),
        child: Column(
          children: <Widget>[
            Expanded(
                child: Card(
                    margin: EdgeInsets.fromLTRB(17, 60.0, 17.0, 110.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    elevation: 4.0,
                    color: Colors.white,
                    child: ListView(shrinkWrap: true, children: <Widget>[
                      Image.asset(
                        'asset/img/video1.png',
                        height: 400,
                        width: MediaQuery.of(context).size.width,
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(21, 10, 0.0, 10),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Container(
                                child: RaisedButton(
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(20.0),
                                  ),
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, 'custom_package');
                                  },
                                  color: Colors.blue,
                                  child: Text(
                                    'editer',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(95, 10, 0.0, 10),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Container(
                                child: RaisedButton(
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(20.0),
                                  ),
                                  onPressed: () {
                                    Navigator.pushNamed(context, 'payer');
                                  },
                                  color: Colors.blue,
                                  child: Text(
                                    'publier',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]))),
          ],
        ),
      )),
    );
  }
}
