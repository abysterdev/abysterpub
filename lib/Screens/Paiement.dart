import 'package:flutter/material.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Paiement extends StatefulWidget {
  @override
  _PaiementState createState() => _PaiementState();
}

class _PaiementState extends State<Paiement> {
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideBar(),
      appBar: GradientAppBar(
        title: Text("AbysterPub"),
        backgroundColorStart: Colors.blue,
        backgroundColorEnd: Colors.teal,
      ),
      body: ListView.builder(
          itemCount: 10,
          itemBuilder: (context, index) {
            final ThemeData theme = Theme.of(context);
            return InkWell(
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                    Row(children: <Widget>[
                      Padding(
                          padding: EdgeInsets.fromLTRB(18.0, 0.0, 0.0, 0.0)),
                      Text(
                        "Nom campagne",
                        style: theme.textTheme.title,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                      ),
                    ]),
                    Row(children: <Widget>[
                      Padding(
                          padding: EdgeInsets.fromLTRB(18.0, 0.0, 0.0, 0.0)),
                      Text(''),
                    ]),
                    Row(children: <Widget>[
                      Padding(
                          padding: EdgeInsets.fromLTRB(18.0, 0.0, 0.0, 0.0)),
                      Expanded(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                            Text(
                              "Nom publicite",
                              style: TextStyle(fontStyle: FontStyle.italic),
                              maxLines: 3,
                            ),
                          ])),
                    ]),

                    //SizedBox(height: 13.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 18.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child: Text(
                                "statut",
                                style: theme.textTheme.subtitle,
                                maxLines: 3,
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                RaisedButton(
                                  onPressed: () {},
                                  child: Text(
                                    'Détails',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  color: Colors.blue,
                                ),
                              ],
                            )
                          ]),
                    ),
                    Divider(
                      thickness: 0.3,
                      color: Colors.black,
                    ),
                  ],
                ),
              ),
            );
          }),
      // Changed code
    );
  }
}
