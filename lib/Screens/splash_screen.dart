import 'package:abysterpub/Screens/SignInScreen.dart';
import 'package:abysterpub/Screens/Welcome.dart';
import 'package:abysterpub/restClient.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreeen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreeen> {
  void navigateUser() async {
    var value = await RestClient.getStringFromStorage('isLoggedIn');
    if (value == 'true') {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => Welcome()));
    } else if (value == '') {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => SignInScreen()));
    }
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 4), () {
      navigateUser();
    });
  }

  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SplashScreen(
          seconds: 8,
          image: new Image.asset("asset/img/téléchargement.jpg"),
          gradientBackground: new LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.blue, Colors.teal]),
          styleTextUnderTheLoader: new TextStyle(),
          photoSize: 100.0,
          loaderColor: Colors.blue,
          loadingText: Text(
            "Be inspired be the best in your field...",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
        ));
  }
}
