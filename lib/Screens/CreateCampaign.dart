import 'package:abysterpub/Helpers/formValidator.dart';
import 'package:abysterpub/Screens/create_adset.dart';
import 'package:abysterpub/models/campagne.dart';
import 'package:abysterpub/states/facebookStates.dart';
import 'package:abysterpub/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:abysterpub/Helpers/dropdown_formfield.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:provider/provider.dart';
import 'package:flutter_flexible_toast/flutter_flexible_toast.dart';

class CreateCampaign extends StatefulWidget {
  @override
  _CreateCampaignState createState() => _CreateCampaignState();
}

class _CreateCampaignState extends State<CreateCampaign> {
  String _mytarget;
  String _mytargetResult;
  String _mygoal;
  String _mygoalResult;
  final formKey = new GlobalKey<FormState>();
  GlobalKey<FormState> _key = new GlobalKey();
  TextEditingController campaignController = new TextEditingController();
  Campaign _campaign = new Campaign();
  FacebookStates face;
  @override
  void initState() {
    super.initState();
    _mytarget = '';
    _mygoal = '';

    _mytargetResult = '';
    _mygoalResult = '';
    _focusNode.addListener(() {
      if (!_focusNode.hasFocus) {
        FocusScope.of(context).requestFocus(_focusNode);
      }
    });
  }

  void showToast(String message, ICON icon) {
    FlutterFlexibleToast.showToast(
        message: message,
        toastLength: Toast.LENGTH_LONG,
        toastGravity: ToastGravity.BOTTOM,
        icon: icon,
        radius: 100,
        elevation: 10,
        imageSize: 35,
        textColor: Colors.white,
        backgroundColor: Colors.black,
        timeInSeconds: 2);
  }

  void campaignTreatment() async {
    if (_key.currentState.validate()) {
      // _key.currentState.save();
      await _campaign
          .createCampaign(campaignController.text, _mygoal, 'PAUSED', 'NONE')
          .then((response) {
        print(campaignController.text);
        print(_mygoal);
        print(response);
        if (response == "error") {
          print("ramses");
          print(response);
          // face.isCampaignLoading = false;
          showToast("Erreur lors de la création de campagne !", ICON.ERROR);
        } else {
          // face.isCampaignLoading = false;
          showToast("Campagne crée avec succès !", ICON.SUCCESS);
          // _campaign.saveCampaign();
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CreateAdset(response)),
          );
        }
      });
    }
  }

  FocusNode _focusNode = FocusNode();

  _saveForm() {
    var form = formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {
        _mytargetResult = _mytarget;
        _mygoalResult = _mygoal;
      });
    }
  }

  // int selectedIndex1 = 0, selectedIndex2 = 0;

  Widget build(BuildContext context) {
    var iskeyboardloaded = MediaQuery.of(context).viewInsets.bottom;
    var hauteurEcran = MediaQuery.of(context).size.height;
    var largeurEcran = MediaQuery.of(context).size.width;
    return ChangeNotifierProvider<FacebookStates>(
        create: (context) => FacebookStates(),
        child: Builder(builder: (context) {
          return Scaffold(
              backgroundColor: Colors.white,
              drawer: SideBar(),
              appBar: GradientAppBar(
                title: Text("AbysterPub"),
                backgroundColorStart: Colors.blue,
                backgroundColorEnd: Colors.teal,
              ),
              body:
                  Consumer<FacebookStates>(builder: (context, provider, child) {
                return AbysterPubLoader(
                    inAsyncCall: provider.isCampaignLoading,
                    child: Center(
                        child: Container(
                            height: MediaQuery.of(context).size.height,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topRight,
                                    end: Alignment.bottomLeft,
                                    colors: [Colors.blue, Colors.teal])),
                            child: Column(children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: (iskeyboardloaded == 0.0)
                                      ? EdgeInsets.fromLTRB(
                                          largeurEcran / 70,
                                          hauteurEcran / 5,
                                          largeurEcran / 70,
                                          hauteurEcran / 5)
                                      : EdgeInsets.fromLTRB(
                                          largeurEcran / 70,
                                          hauteurEcran / 18,
                                          largeurEcran / 70,
                                          hauteurEcran / 105),
                                  child: Container(
                                    margin: EdgeInsets.fromLTRB(
                                        largeurEcran / 22,
                                        60,
                                        largeurEcran / 22,
                                        70),
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      elevation: 4.0,
                                      color: Colors.white,
                                      child: Form(
                                        key: _key,
                                        child: ListView(
                                          shrinkWrap: true,
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: 20,
                                                                  left: 10),
                                                          child: TextFormField(
                                                            // onSaved:
                                                            //     (String value) {
                                                            //   _campaign. =
                                                            //       value;
                                                            // },
                                                            validator:
                                                                FormValidator()
                                                                    .validateOtherFields,
                                                            focusNode:
                                                                _focusNode,
                                                            autofocus: true,
                                                            showCursor: true,
                                                            controller:
                                                                campaignController,
                                                            decoration:
                                                                InputDecoration(
                                                                    hintText:
                                                                        "nom campagne"),
                                                          )))
                                                ],
                                              ),
                                            ),
                                            // Padding(
                                            //   padding: const EdgeInsets.all(10.0),
                                            //   child: Row(
                                            //     children: <Widget>[
                                            //       Expanded(
                                            //         child: Container(
                                            //           margin:
                                            //               EdgeInsets.only(right: 10, left: 8),
                                            //           child: DropDownFormField(
                                            //             hintText: 'cible',
                                            //             value: _mytarget,
                                            //             onSaved: (value) {
                                            //               setState(() {
                                            //                 _mytarget = value;
                                            //               });
                                            //             },
                                            //             onChanged: (value) {
                                            //               setState(() {
                                            //                 _mytarget = value;
                                            //               });
                                            //             },
                                            //             dataSource: [
                                            //               {
                                            //                 "display": "adinterest",
                                            //                 "value": "adinterest",
                                            //               },
                                            //             ],
                                            //             textField: 'display',
                                            //             valueField: 'value',
                                            //           ),
                                            //         ),
                                            //       ),
                                            //       Expanded(
                                            //           child: Container(
                                            //               margin: EdgeInsets.only(
                                            //                   right: 20,
                                            //                   left: 10,
                                            //                   bottom: 18,
                                            //                   top: 38),
                                            //               child: TextField(
                                            //                 showCursor: true,
                                            //                 decoration:
                                            //                     InputDecoration(hintText: ''),
                                            //               ))),
                                            //     ],
                                            //   ),
                                            // ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20, left: 10),
                                                    child: new IgnorePointer(
                                                      ignoring: false,
                                                      child: DropDownFormField(
                                                        // validator: FormValidator()
                                                        //     .validateOtherFields,
                                                        hintText: 'objectifs',
                                                        value: _mygoal,
                                                        onSaved: (value) {
                                                          setState(() {
                                                            _mygoal = value;
                                                            // _campaign.objectif =
                                                            //     value;
                                                          });
                                                        },
                                                        onChanged: (value) {
                                                          setState(() {
                                                            _mygoal = value;
                                                          });
                                                        },
                                                        dataSource: [
                                                          {
                                                            "display":
                                                                "clic de liens",
                                                            "value":
                                                                "LINK_CLICKS",
                                                          },
                                                          {
                                                            "display":
                                                                "installation d'apps",
                                                            "value":
                                                                "APP_INSTALLS",
                                                          },
                                                          {
                                                            "display":
                                                                "faire connaître sa marque",
                                                            "value":
                                                                "BRAND_AWARENESS",
                                                          },
                                                          {
                                                            "display":
                                                                "vente catalogue de produits",
                                                            "value":
                                                                "PRODUCT_CATALOG_SALES",
                                                          },
                                                          {
                                                            "display":
                                                                "Faire visiter sa boutique",
                                                            "value":
                                                                "STORE_VISITS",
                                                          },
                                                          {
                                                            "display":
                                                                "Faire visiter sa boutique",
                                                            "value":
                                                                "VIDEO_VIEWS",
                                                          },
                                                        ],
                                                        textField: 'display',
                                                        valueField: 'value',
                                                      ),
                                                    ),
                                                  )),
                                                ],
                                              ),
                                            ),

                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal:
                                                      largeurEcran / 30),
                                              child: Row(
                                                children: <Widget>[
                                                  ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    child: Container(
                                                      child: RaisedButton(
                                                        shape:
                                                            new RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  20.0),
                                                        ),
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        color: Colors.blue,
                                                        child: Text(
                                                          'Annuler',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 18),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .fromLTRB(
                                                        20, 0.0, 0.0, 0.0),
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      child: Container(
                                                        child: RaisedButton(
                                                          shape:
                                                              new RoundedRectangleBorder(
                                                            borderRadius:
                                                                new BorderRadius
                                                                        .circular(
                                                                    20.0),
                                                          ),
                                                          onPressed: () {
                                                            // provider.isCampaignLoading =
                                                            //     true;
                                                            //campaignTreatment();
                                                            _campaign
                                                                .createCampaign(
                                                                    campaignController
                                                                        .text,
                                                                    _mygoal,
                                                                    'PAUSED',
                                                                    'NONE');
                                                            print(_mygoal);
                                                          },
                                                          color: Colors.blue,
                                                          child: Text(
                                                            'Creer Campagne',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 18),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ]))));
              }));
        }));
  }
}
