import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:abysterpub/widgets/sidebar.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Payer extends StatefulWidget {
  @override
  _PayerState createState() => _PayerState();
}

class _PayerState extends State<Payer> {
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideBar(),
      appBar: GradientAppBar(
        title: Text("AbysterPub"),
        backgroundColorStart: Colors.blue,
        backgroundColorEnd: Colors.teal,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 60.0, horizontal: 2.0),
        child: GridView.count(
          crossAxisCount: 2,
          padding: EdgeInsets.fromLTRB(3, 120, 3, 20),
          children: <Widget>[
            makeDashboardItem("Orange Money", Icons.attach_money, 0),
            makeDashboardItem("Mobile Money", Icons.attach_money, 1),
          ],
        ),
      ),
    );
  }

  Card makeDashboardItem(String title, IconData icon, int i) {
    return Card(
      elevation: 1.0,
      margin: new EdgeInsets.fromLTRB(6, 5, 6, 5),
      child: Container(
        height: 400,
        decoration: BoxDecoration(color: Color.fromRGBO(220, 220, 220, 1.0)),
        child: new InkWell(
          onTap: () {
            switch (i) {
              case 0:
                Navigator.pushNamed(context, 'OMPayment');
                break;
              case 1:
                Navigator.pushNamed(context, 'MOMOPayment');
                break;
              default:
                Navigator.pushNamed(context, '');
            }
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              SizedBox(height: 40.0),
              Center(
                  child: Icon(
                icon,
                size: 40.0,
                color: Colors.black,
              )),
              SizedBox(height: 30.0),
              new Center(
                child: new Text(title,
                    style: new TextStyle(fontSize: 18.0, color: Colors.black)),
              )
            ],
          ),
        ),
      ),
    );
  }
}

//  onImageTap:(){
//
//  },
