import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

const String APPLICATION = 'Tdz65jUm1pZ8ZSwJNJOOU#';
const String WS_USERNAME = 'kouamramses2@gmail.com';
const String WS_PASSWORD = 'kouam';
const String WS_URL = '192.168.137.1:8080';
const CREDENTIALS = {
  'application': APPLICATION,
  'username': WS_USERNAME,
  'password': WS_PASSWORD
};

class RestClient {
  static Map<String, String> getHeaders([String token = '']) {
    Map<String, String> headers;
    if (token == '') {
      headers = {
        'Content-Type': 'Application/json',
        'Accept': 'Application/json'
      };
    } else {
      headers = {
        'Accept': 'Application/json; charset=utf-8',
        'Authorization': 'Bearer $token',
        'Content-Type': 'Application/json; charset=utf-8',
      };
    }
    return headers;
  }

  static Future<String> getAuthToken() async {
    Map<String, String> headers = RestClient.getHeaders();

    return http
        .post("$WS_URL/authenticate",
            body: json.encode(CREDENTIALS), headers: headers)
        .then((http.Response response) {
      if (response.statusCode != 200) {
        throw new Exception(
            "Error while fetching Jwt Token \n ${WSErrorHandler.fromJson(json.decode(response.body)).toJson()}");
      }
      return JwtToken.fromJson(json.decode(response.body)).jwttoken;
    }).catchError((error) {
      print(error);
      return '';
    });
  }

  static Future<int> procesPostRequest(
    String path,
    Map<String, dynamic> postData,
    String token,
  ) {
    Map<String, String> headers = RestClient.getHeaders(token);
    // String apiUrl = await RestClient.getStringFromStorage('api_url');
    // if (apiUrl == "" || apiUrl == null) {
    //   await RestClient.initApiUrls();
    //   apiUrl = await RestClient.getStringFromStorage('api_url');
    //   if (apiUrl == "" || apiUrl == null) {
    //     //customSnackBar(scaffoldKey, "Impossible de communiquer avec le serveur.", backgroundColor: HypidateColor.ceriseRed);
    //     print("Impossible de communiquer avec le serveur:");
    //   }
    // }
    return http
        .post("$WS_URL/$path", body: json.encode(postData), headers: headers)
        .then((http.Response response) {
      if (response.statusCode != 200) {
        print(
            "Error while processing post request \n ${WSErrorHandler.fromJson(json.decode(response.body)).toJson()}");
      }
      return response.statusCode;
    }).catchError((error) {
      print(error);
    });
  }

  static Future<Map<String, dynamic>> procesPostRequest2(
    String path,
    Map<String, dynamic> postData,
    String token,
  ) async {
    Map<String, String> headers = RestClient.getHeaders(token);
    return http
        .post("$WS_URL/$path", body: json.encode(postData), headers: headers)
        .then((http.Response response) {
      if (response.statusCode != 200) {
        print(
            "Error while processing post request \n ${WSErrorHandler.fromJson(json.decode(response.body)).toJson()}");
      }
      return json.decode(response.body);
    });
  }

  static Future<dynamic> procesGetRequest(String path, String token) async {
    Map<String, String> headers = RestClient.getHeaders(token);

    return http
        .get("$WS_URL/$path", headers: headers)
        .then((http.Response response) {
      if (response.statusCode != 200) {
        print(
            "Error while processing get request \n ${WSErrorHandler.fromJson(json.decode(response.body)).toJson()}");
        // List<dynamic> list = [];
        // return Future.value(list);
      }

      return http.Response;
    }).catchError((error) {
      print(error);
    });
  }

  static Future<int> procesGetRequest2(Uri path, String token) async {
    Map<String, String> headers = RestClient.getHeaders(token);

    return http.get("$path", headers: headers).then((http.Response response) {
      if (response.statusCode != 200) {
        print(
            "Error while processing get request \n ${WSErrorHandler.fromJson(json.decode(response.body)).toJson()}");
      }

      return response.statusCode;
    }).catchError((error) {
      print(error);
      List<dynamic> list = [];
      return Future.value(list);
    });
  }

  // static Future<List<dynamic>> procesGetRequest2(
  //     String path, String token) async {
  //   Map<String, String> headers = RestClient.getHeaders(token);

  //   return http
  //       .get("$WS_URL/$path", headers: headers)
  //       .then((http.Response response) {
  //     if (response.statusCode != 200) {
  //       print(
  //           "Error while processing get request \n ${WSErrorHandler.fromJson(json.decode(response.body)).toJson()}");
  //       List<dynamic> list = [];
  //       return Future.value(list);
  //     }
  //     print(json.decode(response.body));
  //     return [json.decode(response.body)];
  //   }).catchError((error) {
  //     print(error);
  //     List<dynamic> list = [];
  //     return Future.value(list);
  //   });
  // }

  static Future<String> persistMapDataInStorage(
      String key, Map<String, dynamic> data) async {
    return SharedPreferences.getInstance().then((prefs) {
      prefs.setString(key, json.encode(data));
      return 'success';
    }).catchError((error) {
      print(error);
      return 'error';
    });
  }

  static Future<String> persistListDataInStorage(
      String key, List<dynamic> data) async {
    return SharedPreferences.getInstance().then((prefs) {
      prefs.setString(key, json.encode(data));
      return 'success';
    }).catchError((error) {
      print(error);
      return 'error';
    });
  }

  static Future<String> persistStringDataInStorage(
      String key, String data) async {
    return SharedPreferences.getInstance().then((prefs) {
      prefs.setString(key, data);
      return 'success';
    }).catchError((error) {
      print(error);
      return 'error';
    });
  }

  static Future<String> persistIntDataInStorage(String key, int data) async {
    return SharedPreferences.getInstance().then((prefs) {
      prefs.setInt(key, data);
      return 'success';
    }).catchError((error) {
      print(error);
      return 'error';
    });
  }

  static Future<String> getStringFromStorage(String key) async {
    return SharedPreferences.getInstance().then((prefs) {
      String value = prefs.getString(key);
      return (value != null) ? value : '';
    }).catchError((error) {
      print(error);
      return '';
    });
  }

  static Future<void> deleteStringFromStorage(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(key);
  }

  static Future<void> clearStorage() async {
    return SharedPreferences.getInstance().then((prefs) {
      prefs.clear();
    });
  }
}

class WSErrorHandler implements Exception {
  String timestamp;
  int status;
  String error;
  String message;
  String path;

  WSErrorHandler();
  WSErrorHandler.full(
      this.timestamp, this.status, this.error, this.message, this.path);

  factory WSErrorHandler.fromJson(Map<String, dynamic> json) {
    return WSErrorHandler.full(json['timestamp'], json['status'], json['error'],
        json['message'], json['path']);
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'timestamp': this.timestamp,
      'status': this.status,
      'error': this.error,
      'message': this.message,
      'path': this.path
    };
  }

  String getMessage() {
    return this.message != '' ? this.message : 'Erreur : problème interne';
  }

  String toString() {
    return json.encode(this.toJson());
  }
}

class JwtToken {
  String jwttoken;

  JwtToken(this.jwttoken);

  factory JwtToken.fromJson(Map<String, dynamic> json) {
    return JwtToken(json['jwttoken']);
  }
}
