import 'package:abysterpub/Screens/create_adset.dart';
import 'package:abysterpub/states/authProvider.dart';
import 'package:abysterpub/states/customPackageState.dart';
import 'package:flutter/material.dart';
import 'package:abysterpub/Screens/CreateCampaign.dart';
import 'package:abysterpub/Screens/CreatePub.dart';
import 'package:abysterpub/Screens/Mes_Pubs.dart';
import 'package:abysterpub/Screens/PubDetails.dart';
import 'package:abysterpub/Screens/create_pub.dart';
import 'package:abysterpub/Screens/custom_package.dart';
import 'package:abysterpub/Screens/packages.dart';
import 'package:abysterpub/Screens/HomeScreen.dart';
import 'package:abysterpub/Screens/profile.dart';
import 'package:abysterpub/Screens/splash_screen.dart';
import 'package:abysterpub/Screens/Notifications.dart';
import 'package:provider/provider.dart';

import 'Screens/MOMOPayment.dart';
import 'Screens/OMPayment.dart';
import 'Screens/Paiement.dart';
import 'Screens/Payer.dart';
import 'Screens/Publish.dart';
import 'Screens/Welcome.dart';
import 'Screens/SignInScreen.dart';
import 'Screens/SignUPScreen.dart';
import 'Screens/passwordReset.dart';
import 'Screens/spot.dart';
import 'Screens/valider_code.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AuthProvider>(create: (_) => AuthProvider()),
          ChangeNotifierProvider<CustomPackageState>(
              create: (_) => CustomPackageState()),
        ],
        child: MaterialApp(
          title: "e-Mpata",
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: SplashScreeen(),
          routes: {
            'SignIn': (context) => SignInScreen(),
            'SignUp': (context) => SignUpScreen(),
            'Home': (context) => HomeScreen(),
            'Welcome': (context) => Welcome(),
            'package': (context) => Packages(),
            'CreateCampaign': (context) => CreateCampaign(),
            'CreatePub': (context) => CreatePub(),
            'SplashScreeen': (context) => SplashScreeen(),
            //'create_pub': (context) => Create_pub(),
            'profile': (context) => Profile(),
            'mes_pubs': (context) => Mespubs(),
            'pubDetails': (context) => PubDetails(),
            'notifications': (context) => Notifications(),
            'custom_package': (context) => Custom_Package(),
            'spot': (context) => Spot(),
            'paiement': (context) => Paiement(),
            'publish': (context) => Publish(),
            'payer': (context) => Payer(),
            'OMPayment': (context) => OMPayment(),
            'MOMOPayment': (context) => MOMOPayment(),
            'valider_code': (context) => Valider_Payment(),
            //'adset': (context) => CreateAdset(id),
            'passwordReset': (context) => PasswordReset(),
          },
        ));
  }
}
